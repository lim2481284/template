<?php

namespace App\Http\Controllers\Template;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AuthController extends Controller
{

    //Login page
    public function loginPage(){
        return view('dashboard.auth.login');
    }

}
