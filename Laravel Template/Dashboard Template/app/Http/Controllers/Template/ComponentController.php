<?php

namespace App\Http\Controllers\Template;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ComponentController extends Controller
{

    //Component page - Widget
    public function componentWidget(){
        return view('template.component.widget');
    }


    //Component page - List
    public function componentList(){
        return view('template.component.list');
    }

    //Component page - Typo
    public function componentTypo(){
        return view('template.component.typo');
    }

    //Component page - Input
    public function componentInput(){
        return view('template.component.input');
    }

    //Component page - Scroll
    public function componentScroll(){
        return view('template.component.scroll');
    }

    //Component page - Button
    public function componentButton(){
        return view('template.component.button');
    }

}
