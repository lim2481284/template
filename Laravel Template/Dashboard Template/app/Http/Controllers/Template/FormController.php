<?php

namespace App\Http\Controllers\Template;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FormController extends Controller
{

    //Form page - Basic
    public function basicForm(){
        return view('template.form.basic');
    }


    //Form page - Advanced
    public function advancedForm(){
        return view('template.form.advanced');
    }


    public function fileUpload(Request $request){
        return dd(1);
    }

}
