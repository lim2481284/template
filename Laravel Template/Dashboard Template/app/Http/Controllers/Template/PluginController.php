<?php

namespace App\Http\Controllers\Template;

use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PluginController extends Controller
{

    //Chart page
    public function chart(){
        return view('template.plugin.chart');
    }


    //Map page
    public function map(){
        return view('template.plugin.map');
    }


    //Tree page
    public function tree(){
        return view('template.plugin.tree');
    }

    //Icons page
    public function icons(){
        return view('template.plugin.icons');
    }

    //Popup page
    public function popup(){
        return view('template.plugin.popup');
    }

    //Carousel page
    public function carousel(){
        return view('template.plugin.carousel');
    }


}
