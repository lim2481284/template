<?php

namespace App\Http\Controllers\Template;


use DB;
use Mail;
use Auth;

use App\Sample;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Controller;

use League\Csv\Reader;
use Carbon\Carbon;
use Spatie\DbDumper\Databases\MySql;
use Barryvdh\DomPDF\Facade as PDF;

class TableController extends Controller
{

    //Table page - Basic table
    public function basicTable(){
        return view('template.table.basic');
    }


    //Table page - Form table
    public function formTable(Request $request){
        $results = Sample::paginate(4);
        if($query=$request->searchQuery)
        {

            /* Sample Data Table Search Query */
            $results = Sample::where('name', 'like', "%$query%")
                    ->orWhere('data', 'like', "%$query%")
                    ->paginate(4);


            /* Advanced search (AND & OR) : select * from sample where id = 1 and ( name = ... or data = ... ) */
            $searchQuery = $query;
            $results = Sample::where('id',"1")->where(function ($query) use ($searchQuery) {
                    $query->where('name', 'like', "%$query%")
                          ->orWhere('data', 'like', "%$query%");
            })->orderBy('created_at','desc')->paginate(20);

        }
        $total = $results->count();
        return view('template.table.form',compact('results','query','total'));
    }


    //Table page - Form table design 2
    public function formTable2(Request $request){
        $results = Sample::paginate(4);
        if($query=$request->searchQuery)
        {

            /* Sample Data Table Search Query */
            $results = Sample::where('name', 'like', "%$query%")
                    ->orWhere('data', 'like', "%$query%")
                    ->paginate(4);

        }
        $total = $results->count();
        return view('template.table.form2',compact('results','query','total'));
    }


    //Table page - Data table
    public function dataTable(Request $request){

        $results = Sample::paginate(4);
        if($query=$request->searchQuery)
        {

            /* Sample Data Table Search Query */
            $results = Sample::where('name', 'like', "%$query%")
                    ->orWhere('data', 'like', "%$query%")
                    ->paginate(4);

        }
        $total = $results->count();
        return view('template.table.data',compact('results','query','total'));
    }


    //Export to CSV Function
    public function exportCSV(Request $request){

        //Get request data
        $csvExporter = new \Laracsv\Export();
        $startDate = date($request->startDate);
        $endDate =  date('Y-m-d', strtotime('+1 day', strtotime($request->endDate)));
        $limit = $request->limit;

        //If have limit
        if($limit)
            $records = Sample::where('created_at','>=',$startDate)->where('created_at','<=',$endDate)->take($limit)->get();
        else
            $records =  Sample::where('created_at','>=',$startDate)->where('created_at','<=',$endDate)->get();

        //Filtering CSV data
        $csvExporter->beforeEach(function ($records) {
            //CSV avoit long string number become integer (zero trailing)
            $records->data = (string)("=\"".$records->data."\"");
        });

        //Export to csv
        $csvExporter->build($records, [
            'id' => 'id',
            'name' => 'Name',
            'data' => 'Data'
        ])->download();

    }


    //Export to PDF Function
    public function exportPDF(Request $request){

        //Get request data
        $startDate = date($request->startDate);
        $endDate =  date('Y-m-d', strtotime('+1 day', strtotime($request->endDate)));
        $limit = $request->limit;

        //If have limit
        if($limit)
            $records = Sample::where('created_at','>=',$startDate)->where('created_at','<=',$endDate)->take($limit)->get();
        else
            $records =  Sample::where('created_at','>=',$startDate)->where('created_at','<=',$endDate)->get();

        //Get template and generate PDF
        $pdf = PDF::loadView('template.table.pdf_template', compact('records'));
        return $pdf->download('Sample.pdf');

    }


    //Display imported csv data
    public function importCSVView(Request $request)
    {
        $name = $request->file('csvFile')->getClientOriginalName();
        $path = $request->csvFile->storeAs('excel',"$name");
        $csv = Reader::createFromPath("uploads/$path", 'r');
        $row = $csv->jsonSerialize();
        array_shift($row);

        return view('template.table.import-view',compact('row','path'));
    }


    //Import CSV to user record
    public function importCSV(Request $request)
    {

        //Get CSV content
        $path = $request->path;
        $csv = Reader::createFromPath("uploads/$path", 'r');
        $row = $csv->jsonSerialize();
        array_shift($row);

        /* Backup SQL ( Only work on server )

        $now = Carbon::now();
        $now = $now->toDateString();
        Mysql::create()
            ->setDbName(env("DB_DATABASE"))
            ->setUserName(env('DB_USERNAME'))
            ->setPassword(env('DB_PASSWORD'))
            ->includeTables(['sample'])
            ->dumpToFile("backup/sample_$now.sql");

        */

        //Import CSV record
        foreach($row as $record)
        {
            Sample::create(['name'=>$record[0], 'data'=>$record[1]]);
        }

         return redirect('/template/data-table')->with('status','CSV Imported');
    }


    //Cancel import
    public function cancelImport(Request $request)
    {

        //Remove uploaded CSV file
        $path = $request->path;
        Storage::delete("$path");

        return redirect('/template/data-table')->with('status','Import CSV cancelled');
    }

}
