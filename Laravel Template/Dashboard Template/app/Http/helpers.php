<?php


//Function to get locazliation code
function getSupportedLocalesNative()
{
    $arr = [];
    foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
    {
        $arr_list = [LaravelLocalization::getLocalizedURL($localeCode, null, [], true)=>$properties['native']];
        $arr= array_merge((array)$arr,(array)$arr_list);
    }
    return $arr;
}
