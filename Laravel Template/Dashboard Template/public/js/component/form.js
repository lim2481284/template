$(document).ready(function(){

    //Advanced Form
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $('div#froala-editor').froalaEditor({
        toolbarStickyOffset: 100,
        width: '100%',
        widthMax: '100%',
        imageMove: true,
        imageUploadParam: 'file',
        imageUploadURL: '/dashboard/data-table/import/view',
        imageUploadParams: {
            _token: CSRF_TOKEN,
            froala: 'true'
        }
    })

})
