

$(document).ready(function(){

    //Datepicker
    $('.datepicker').flatpickr();

    //Data table function
    var width=window.innerWidth;
    var height=window.innerHeight;
    $('.ui.dropdown').dropdown();
    $('.table-container').css('max-width', width*0.72);
    $('#table tfoot th').each( function (item,i) {
        var title = $(this).text();
        if ($(this).html()==="Status"){
          $(this).html(`
            <select style="border:1px solid rgba(34,36,38,.15);padding:.5em">
              <option value="">All</option>
              <option value="failed">Failed</option>
              <option value="completed">Completed</option>
              <option value="pending">Pending</option>
            </select>`)
        }
        else if ($(this).html()==="Type"){
          $(this).html(`
            <select style="border:1px solid rgba(34,36,38,.15);padding:.5em">
              <option value="">All</option>
              <option value="transfer to card">Transfer to Card</option>
              <option value="transfer to member">Transfer to Member</option>
              <option value="transfer to company">Transfer to Company</option>
            </select>`)
        }
        else if ($(this).html()){
          $(this).html( `
            <div class="ui input" style="margin:auto;display:block">
              <input type="text" placeholder="`+title+`" id="`+item+"-search"+`" />
            </div>
          `);
        }
        else{
          $(this).html( `
            <div class="ui input">
            </div>
          `);
        }
    });
    var table=$("#table").DataTable( {
        columnDefs: [
        { width: "5%", targets: 0 }
        ],
        fixedColumns: true
    });

    // Data table search function
    table.columns().every( function () {
       var that = this;
       $('input', this.footer() ).on( 'keyup change', function () {
           if ( that.search() !== this.value ) {
               that.search( this.value ).draw();
           }
       } );

       $('select', this.footer() ).on( 'change', function () {
           if ( that.search() !== this.value ) {
               that.search( this.value ).draw();
           }
       } );
    });
    var r = $('#table tfoot tr');
    r.find('th').each(function(){
        $(this).css('padding', 8);
    });
    $('#table thead').append(r);
    $('#search_0').css('text-align', 'center');

    //On select filter
    $(document).on('change','.filter-select',function(){
        var filter = $(this).val();
        showLoader();
        window.location.href="?filter="+filter;
    });

    //Onclick more detail button
    $(document).on('click','.action-btn',function(){
        var transactionID = $(this).attr('data-transaction-id');
        $("[name='transactionID']").val(transactionID);
        $.ajax({
            type: 'GET',
            url: "/dashboard/transaction/"+transactionID,
            async: false,
            success: function(data) {
                //Insert Details
                $('.transaction_card').html(data.card);
                $('.transaction_username').html(data.creator);
                $('.transaction_amount').html(data.amount);
                $('.transaction_card_balance').html(data.card_balance);
                $('.transaction_deducted_amount').html(data.deducted_amount);
                $('.transaction_rate').html(data.rate );
                $('.status').html(data.status);
                $('.transaction_created').html(data.created_at);
                $('.transaction_status').val(data.status);
                $('.transaction_status').change();
                $('.transaction_note').val(data.transaction_note);
            }
        })
    });

    $(document).on('click','.save-btn',function(){
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        var transactionID = $('.action_transactionID').val();
        var transactionStatus = $('.transaction_status').val();
        var transactionNote = $('.transaction_note').val();
        $('.action-inpage-loader').fadeIn('fast');
        $.ajax({
            type: 'POST',
            url: "/dashboard/transaction/update",
            data: {
                _token: CSRF_TOKEN,
                transactionID:transactionID,
                transactionStatus:transactionStatus,
                transactionNote:transactionNote
            },
            dataType: 'JSON',
            success: function(data) {
                $('.action-inpage-loader').fadeOut('fast');
                swal(data.message).then((value) => {
                    $("#actionModal").modal("hide");
                }).then((value) => {
                  window.location.reload(true);
                });;
            },
            error: function(xhr, textStatus, errorThrown){
               swal('Something is wrong, please contact developer');
            }
        });
    });
})
