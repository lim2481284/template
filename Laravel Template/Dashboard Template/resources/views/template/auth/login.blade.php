<!doctype html>
<html lang="{{ app()->getLocale() }}">

<head>
    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>{{env("APP_NAME")}} Dashboard</title>
    <!-- Meta End-->

    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Raleway" />
    <link rel="shortcut icon" href='/img/logo/{{env("APP_LOGO", "logo")}}.ico'/>
    <link href="/css/plugin/dashboard_bootstrap.min.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
    <link href="/css/dashboard/login.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
    <!-- Styles end -->

    <!-- Script -->
    <script type="text/javascript" src="/js/plugin/dashboard_jquery.min.js{{ config('app.link_version') }}"></script>
    <script type="text/javascript" src="/js/plugin/dashboard_bootstrap.min.js{{ config('app.link_version') }}"></script>

    <!-- Script End -->

</head>

<body>

    <!-- Body Content -->
    <canvas id="canv" ></canvas>


</body>
<script type="text/javascript" src="/js/dashboard/login.js{{ config('app.link_version') }}"></script>
</html>
