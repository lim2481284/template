@extends("template.layout.".env("APP_LAYOUT", "default"))

@section('head')
<link href="/css/page/component.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
<link href="/css/component/button.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
@endsection

@section('content')

	<section>
		<div class="row button-section" style="height: 200px">
			<h1>Button 1</h1>
			<div>
				<a href="#" class="button">
			      <div class="qube">
			         <div class="front">now click!</div>
			         <div class="back">mouse over me!</div>
			      </div>
			   </a>
			</div>
		</div>
		<div class="row button-section" style="height: 250px">
			<h1>Button 2</h1>
			<div class="row">
				<button class="btn2 first">Button 1</button>
				<button class="btn2 second">Button 2</button>
				<button class="btn2 third">Button 3</button>
				<button class="btn2 fourth">Button 4</button>
				<button class="btn2 fifth">Button 5</button>
				<button class="btn2 sixth">Button 6</button>
			</div>
		</div>
		<div class="row button-section" style="height: 100px">
			<h1>Button 3</h1>
			<div class="row">
				<div id="btn-download">
				  	<svg width="22px" height="16px" viewBox="0 0 22 16">
					    <path d="M2,10 L6,13 L12.8760559,4.5959317 C14.1180021,3.0779974 16.2457925,2.62289624 18,3.5 L18,3.5 C19.8385982,4.4192991 21,6.29848669 21,8.35410197 L21,10 C21,12.7614237 18.7614237,15 16,15 L1,15" id="check"></path>
					    <polyline points="4.5 8.5 8 11 11.5 8.5" class="svg-out"></polyline>
					    <path d="M8,1 L8,11" class="svg-out"></path>
					</svg>
				</div>
				<div class="btn-circle-download">
					<svg id="arrow" width="14px" height="20px" viewBox="17 14 14 20">
						<path d="M24,15 L24,32"></path>
						<polyline points="30 27 24 33 18 27"></polyline>
					</svg>
					<svg id="check" width="21px" height="15px" viewBox="13 17 21 15">
						<polyline points="32.5 18.5 20 31 14.5 25.5"></polyline>
					</svg>
					<svg id="border" width="48px" height="48px" viewBox="0 0 48 48">
						<path d="M24,1 L24,1 L24,1 C36.7025492,1 47,11.2974508 47,24 L47,24 L47,24 C47,36.7025492 36.7025492,47 24,47 L24,47 L24,47 C11.2974508,47 1,36.7025492 1,24 L1,24 L1,24 C1,11.2974508 11.2974508,1 24,1 L24,1 Z"></path>
					</svg>
				</div>
			</div>
		</div>
		<div class="row button-section" style="height: 520px">
			<h1>Button 4</h1>
			<div class="_btn4 row">
				<div class="flex">
					<a href="#0" class="bttn">Continue</a>
				</div>
				<div class="flex dark">
					<a href="#0" class="bttn-dark">Continue</a>
				</div>
			</div>
		</div>
		<div class="row button-section" style="height: 200px">
			<h1>Button 5</h1>
			<div class="_btn5">
				<div class="flex5">
				  <a href="" class="bttn5">More</a>
				</div>
			</div>
		</div>
		<div class="row button-section" style="height: 200px">
			<h1>Button 6</h1>
			<div class="_btn6">
				<button class="button6">
					<span class="button6__inner">3D Button</span>
				</button>

				<button class="button6 button6--secondary">
					<span class="button6__inner">Submit</span>
				</button>
			</div>
		</div>
		<div class="row button-section" style="height: 200px; background-color: #1F3944;">
			<h1 style="color: white">Button 7</h1>
			<div class="_btn7">
				<svg style="position: absolute; width: 0; height: 0;" width="0" height="0" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
				    <defs>
				        <symbol id="icon-paperplane" viewBox="0 0 1024 1024">
				            <title>paperplane</title>
				            <path class="path1" d="M1009.376 5.12c-5.312-3.424-11.36-5.12-17.376-5.12-6.176 0-12.384 1.76-17.76 5.376l-960 640c-9.888 6.56-15.328 18.112-14.048 29.952 1.216 11.808 8.896 22.016 19.936 26.368l250.368 100.192 117.728 206.016c5.632 9.888 16.096 16 27.424 16.128 0.128 0 0.224 0 0.352 0 11.232 0 21.664-5.952 27.424-15.552l66.464-110.816 310.24 124.064c3.808 1.536 7.808 2.272 11.872 2.272 5.44 0 10.816-1.376 15.68-4.128 8.448-4.736 14.24-13.056 15.872-22.624l160-960c2.080-12.576-3.488-25.184-14.176-32.128zM100.352 664.864l741.6-494.432-539.2 577.184c-2.848-1.696-5.376-3.936-8.512-5.184l-193.888-77.568zM326.048 770.112c-0.064-0.128-0.16-0.192-0.224-0.32l606.176-648.8-516.768 805.184-89.184-156.064zM806.944 947.488l-273.312-109.312c-6.496-2.56-13.248-3.424-19.936-3.808l420.864-652.416-127.616 765.536z"></path>
				        </symbol>
				    </defs>
				</svg>

				<a href="http://panfareast.com/" target="_blank" class="contact-button">
				    Contact 
				    <svg class="icon icon-paperplane"><use xlink:href="#icon-paperplane"></use></svg>
				    <span>shark.pfe@gmail.com</span>
				</a>
			</div>
		</div>
		<div class="row button-section" style="height: 300px">
			<h1>Button 8</h1>
			<div class="_btn8">
				<figure class="circle8">
				  <div class="mask-b"><span class="ok-btn">OK</span>
				    <div class="cursor8"></div>
				  </div>
				</figure>
			</div>
		</div>
		<div class="row button-section" style="height: 300px; background: #222a2c;">
			<h1 style="color: white">Button 9</h1>
			<div class="_btn9">
				<div>
					<a href="#" class="btn9 red">Red</a>
					<a href="#" class="btn9 green">Green</a>
					<a href="#" class="btn9 blue">Blue</a>
				</div>

				<div class="social-buttons">
					<a href="#" class="social-btn entypo-tumblr" target="_blank">
						<div class="sr">Tumblr - opens in new tab</div>
					</a>

					<a href="#" class="social-btn entypo-twitter" target="_blank">
						<div class="sr">Twitter - opens in new tab</div>
					</a>

					<a href="#" class="social-btn entypo-facebook" target="_blank">
						<div class="sr">Facebook - opens in new tab</div>
					</a>

					<a href="#" class="social-btn entypo-instagrem" target="_blank">
						<div class="sr">Instagram - opens in new tab</div>
					</a>
				</div>
			</div>
		</div>
		<div class="row button-section" style="height: 200px">
			<h1>Button 1</h1>
			<div>
				
			</div>
		</div>
		<div class="row button-section" style="height: 200px">
			<h1>Button 1</h1>
			<div>
				
			</div>
		</div>
		<div class="row button-section" style="height: 200px">
			<h1>Button 1</h1>
			<div>
				
			</div>
		</div>
	</section>

<script type="text/javascript">
	$("#btn-download").click(function() {
	  $(this).toggleClass("downloaded");
	});

	$(".btn-circle-download").click(function() {
	  $(this).addClass("load");
	  setTimeout(function() {
	    $(".btn-circle-download").addClass("done");
	  }, 1000);
	  setTimeout(function() {
	    $(".btn-circle-download").removeClass("load done");
	  }, 5000);
	});

	$('.btn9').prepend('<div class="hover"><span></span><span></span><span></span><span></span><span></span></div>');

	$('.social-btn').prepend('<div class="hover"><span></span><span></span><span></span><span></span></div>');

</script>

@stop
