<div class='row chart-row'>
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <canvas id="transferCardChart" ></canvas>
            </div>
        </div>
    </div>
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <canvas id="transferCompanyChart" ></canvas>
            </div>
        </div>
    </div>
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <canvas id="transferMemberChart" ></canvas>
            </div>
        </div>
    </div>
</div>


<script>
var ctx_card = document.getElementById("transferCardChart");
var cardChart = new Chart(ctx_card, {
    type: 'bar',
    maintainAspectRatio: false,
    data: {
        labels: [
            @foreach($cardTransactionChart as $card)
                "{{$card->date}}",
            @endforeach
        ],
        datasets: [{
            label: 'Daily Transfer to Card',
            data: [
                @foreach($cardTransactionChart as $card)
                    "{{$card->sum}}",
                @endforeach
            ],
            backgroundColor: [
                @foreach($cardTransactionChart as $card)
                    'rgba(255, 99, 132, 0.2)',
                @endforeach
            ],
            borderColor: [
                @foreach($cardTransactionChart as $card)
                    'rgba(255,99,132,1)',
                @endforeach
            ],
            borderWidth: 1
        }]
    },
    options: {
        maintainAspectRatio: false,
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});


var ctx_company = document.getElementById("transferCompanyChart");
var companyChart = new Chart(ctx_company, {
    type: 'bar',
    maintainAspectRatio: false,
    data: {
        labels: [
            @foreach($companyTransactionChart as $card)
                "{{$card->date}}",
            @endforeach
        ],
        datasets: [{
            label: 'Daily Transfer to Company',
            data: [
                @foreach($companyTransactionChart as $card)
                    "{{$card->sum}}",
                @endforeach
            ],
            backgroundColor: [
                @foreach($companyTransactionChart as $card)
                     'rgba(153, 102, 255, 0.2)',
                @endforeach
            ],
            borderColor: [
                @foreach($companyTransactionChart as $card)
                    'rgba(153, 102, 255, 1)',
                @endforeach
            ],
            borderWidth: 1
        }]
    },
    options: {
        maintainAspectRatio: false,
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});


var ctx_member = document.getElementById("transferMemberChart");
var memberChart = new Chart(ctx_member, {
    type: 'bar',
    maintainAspectRatio: false,
    data: {
        labels: [
            @foreach($transferTransactionChart as $card)
                "{{$card->date}}",
            @endforeach
        ],
        datasets: [{
            label: 'Daily Transfer to Member',
            data: [
                @foreach($transferTransactionChart as $card)
                    "{{$card->sum}}",
                @endforeach
            ],
            backgroundColor: [
                @foreach($transferTransactionChart as $card)
                     'rgba(54, 162, 235, 0.2)',
                @endforeach
            ],
            borderColor: [
                @foreach($transferTransactionChart as $card)
                    'rgba(54, 162, 235, 1)',
                @endforeach
            ],
            borderWidth: 1
        }]
    },
    options: {
        maintainAspectRatio: false,
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});


</script>
