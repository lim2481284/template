@extends("template.layout.".env("APP_LAYOUT", "default"))

@section('head')

<link href="/css/page/component.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
<link rel="stylesheet" type="text/css" href="/css/component/input.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/prefixfree/1.0.7/prefixfree.min.js"></script>

<link href='https://fonts.googleapis.com/css?family=Alegreya+Sans:300,400|Source+Sans+Pro:400,300' rel='stylesheet' type='text/css'>
<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-beta.2/css/bootstrap-grid.css'>

@endsection

@section('content')

<section class="bgcolor">
    <div class="row fill-box" style="height: 750px">
        <div class="checkout-box">
            <h1 class="checkout-title">Checkout Form</h1>
            <form class="form cf">
                <section class="plan cf">
                    <h2>Choose a plan:</h2>
                    <input type="radio" name="radio1" id="free" value="free"><label class="free-label four col" for="free">Free</label>
                    <input type="radio" name="radio1" id="basic" value="basic" checked><label class="basic-label four col" for="basic">Basic</label>
                    <input type="radio" name="radio1" id="premium" value="premium"><label class="premium-label four col" for="premium">Premium</label>
                </section>
                <section class="payment-plan cf">
                    <h2>Select a payment plan:</h2>
                    <input type="radio" name="radio2" id="monthly" value="monthly" checked><label class="monthly-label four col" for="monthly">Monthly</label>
                    <input type="radio" name="radio2" id="yearly" value="yearly"><label class="yearly-label four col" for="yearly">Yearly</label>
                </section>
                <section class="payment-type cf">
                    <h2>Select a payment type:</h2>
                    <input type="radio" name="radio3" id="credit" value="credit"><label class="credit-label four col" for="credit">Credit Card</label>
                    <input type="radio" name="radio3" id="debit" value="debit"><label class="debit-label four col" for="debit">Debit Card</label>
                    <input type="radio" name="radio3" id="paypal" value="paypal" checked><label class="paypal-label four col" for="paypal">Paypal</label>
                </section>  
                <input class="checkout-submit" type="submit" value="Submit">     
            </form>
        </div>
    </div>
    <div class="row fill-box" style="height: 500px; background-color: lightblue;">
        <h3>Custom 1</h3>
        <div class="custom__1">
            <div class="control-group">
                <h1>Checkboxes</h1>
                <label class="control control--checkbox">First checkbox
                    <input type="checkbox" checked="checked"/>
                    <div class="control__indicator"></div>
                </label>
                <label class="control control--checkbox">Second checkbox
                    <input type="checkbox"/>
                    <div class="control__indicator"></div>
                </label>
                <label class="control control--checkbox">Disabled
                    <input type="checkbox" disabled="disabled"/>
                    <div class="control__indicator"></div>
                </label>
                <label class="control control--checkbox">Disabled & checked
                    <input type="checkbox" disabled="disabled" checked="checked"/>
                    <div class="control__indicator"></div>
                </label>
            </div>
            <div class="control-group">
                <h1>Radio buttons</h1>
                <label class="control control--radio">First radio
                    <input type="radio" name="radio" checked="checked"/>
                    <div class="control__indicator"></div>
                </label>
                <label class="control control--radio">Second radio
                    <input type="radio" name="radio"/>
                    <div class="control__indicator"></div>
                </label>
                <label class="control control--radio">Disabled
                    <input type="radio" name="radio2" disabled="disabled"/>
                    <div class="control__indicator"></div>
                </label>
                <label class="control control--radio">Disabled & checked
                    <input type="radio" name="radio2" disabled="disabled" checked="checked"/>
                    <div class="control__indicator"></div>
                </label>
            </div>
            <div class="control-group">
                <h1>Select boxes</h1>
                <div class="select1">
                    <select>
                        <option>First select</option>
                        <option>Option</option>
                        <option>Option</option>
                    </select>
                    <div class="select1__arrow"></div>
                </div>
                <div class="select1">
                    <select>
                        <option>Second select</option>
                        <option>Option</option>
                        <option>Option</option>
                    </select>
                    <div class="select1__arrow"></div>
                </div>
                <div class="select1">
                    <select disabled="disabled">
                        <option>Disabled</option>
                        <option>Option</option>
                        <option>Option</option>
                    </select>
                    <div class="select1__arrow"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="row fill-box">
        <h3>Custom 2</h3>
        <div class="custom__2">
            <div class="align-center">
                <form class="form2">
                    <input type="email" class="form2__field" placeholder="Your E-Mail Address" />
                    <button type="button" class="btn2 btn2--primary btn2--inside uppercase">Subscribe</button>
                </form>
            </div>
        </div>
    </div>
    <div class="row fill-box" style="height: 800px; background-color: #ff4a56;">
        <h3>Custom 1</h3>
        <div class="custom__3">
            <form class="form3">
                <h1>Fancy Text Inputs</h1>
                <div class="form3 question">
                    <input type="text" required/>
                    <label>First Name</label>
                </div>
                <div class="form3 question">
                    <input type="text" required/>
                    <label>Last Name</label>
                </div>
                <div class="form3 question">
                    <input type="text" required/>
                    <label>Email Address</label>
                </div>
                <div class="form3 question">
                    <input type="text" required/>
                    <label>Email Confirm</label>
                </div>
                <button>Submit</button>
            </form>
        </div>
    </div>
    <div class="row fill-box" style="height: 700px; background: url('https://www.toptal.com/designers/subtlepatterns/patterns/geometry2.png');">
        <h3>Custom 4</h3>
        <div class="custom__4">
            <div class="container text4">
                <h2 class="text4"><i>Border effects</i></h2>
                <div class="row text4">
                    <div class="col-3">
                        <input class="effect-1" type="text" placeholder="Placeholder Text">
                        <span class="focus-border"></span>
                    </div>
                    <div class="col-3">
                        <input class="effect-2" type="text" placeholder="Placeholder Text">
                        <span class="focus-border"></span>
                    </div>
                    <div class="col-3">
                        <input class="effect-3" type="text" placeholder="Placeholder Text">
                        <span class="focus-border"></span>
                    </div>        
                    <div class="col-3">
                        <input class="effect-4" type="text" placeholder="Placeholder Text">
                        <span class="focus-border"></span>
                    </div>
                    <div class="col-3">
                        <input class="effect-5" type="text" placeholder="Placeholder Text">
                        <span class="focus-border"></span>
                    </div>
                    <div class="col-3">
                        <input class="effect-6" type="text" placeholder="Placeholder Text">
                        <span class="focus-border"></span>
                    </div>        
                    <div class="col-3">
                        <input class="effect-7" type="text" placeholder="Placeholder Text">
                        <span class="focus-border">
                            <i></i>
                        </span>
                    </div>
                    <div class="col-3">
                        <input class="effect-8" type="text" placeholder="Placeholder Text">
                        <span class="focus-border">
                            <i></i>
                        </span>
                    </div>
                    <div class="col-3">
                        <input class="effect-9" type="text" placeholder="Placeholder Text">
                        <span class="focus-border">
                            <i></i>
                        </span>
                    </div>
                </div>
                <h2><i>Input with Label Effects</i></h2>
                <div class="row">
                    <div class="col-3 input-effect">
                        <input class="effect-16" type="text" placeholder="">
                        <label>First Name</label>
                        <span class="focus-border"></span>
                    </div>
                    <div class="col-3 input-effect">
                        <input class="effect-17" type="text" placeholder="">
                        <label>First Name</label>
                        <span class="focus-border"></span>
                    </div>
                    <div class="col-3 input-effect">
                        <input class="effect-18" type="text" placeholder="">
                        <label>First Name</label>
                        <span class="focus-border"></span>
                    </div>
                    <div class="col-3 input-effect">
                        <input class="effect-19" type="text" placeholder="">
                        <label>First Name</label>
                        <span class="focus-border">
                            <i></i>
                        </span>
                    </div>
                    <div class="col-3 input-effect">
                        <input class="effect-20" type="text" placeholder="">
                        <label>First Name</label>
                        <span class="focus-border">
                            <i></i>
                        </span>
                    </div>
                    <div class="col-3 input-effect">
                        <input class="effect-21" type="text" placeholder="">
                        <label>First Name</label>
                        <span class="focus-border">
                            <i></i>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row fill-box">
        <h3>Custom 5</h3><br>
        <div class="cntr row">
            <div class="col-lg-4">
            <label for="rdo-1" class="btn-radio">
                <input type="radio" id="rdo-1" name="radio-grp">
                <svg width="20px" height="20px" viewBox="0 0 20 20">
                    <circle cx="10" cy="10" r="9"></circle>
                    <path d="M10,7 C8.34314575,7 7,8.34314575 7,10 C7,11.6568542 8.34314575,13 10,13 C11.6568542,13 13,11.6568542 13,10 C13,8.34314575 11.6568542,7 10,7 Z" class="inner"></path>
                    <path d="M10,1 L10,1 L10,1 C14.9705627,1 19,5.02943725 19,10 L19,10 L19,10 C19,14.9705627 14.9705627,19 10,19 L10,19 L10,19 C5.02943725,19 1,14.9705627 1,10 L1,10 L1,10 C1,5.02943725 5.02943725,1 10,1 L10,1 Z" class="outer"></path>
                </svg>
                <span>Option One</span>
            </label></div>
            <div class="col-lg-4">
            <label for="rdo-2" class="btn-radio">
                <input type="radio" id="rdo-2" name="radio-grp">
                <svg width="20px" height="20px" viewBox="0 0 20 20">
                    <circle cx="10" cy="10" r="9"></circle>
                    <path d="M10,7 C8.34314575,7 7,8.34314575 7,10 C7,11.6568542 8.34314575,13 10,13 C11.6568542,13 13,11.6568542 13,10 C13,8.34314575 11.6568542,7 10,7 Z" class="inner"></path>
                    <path d="M10,1 L10,1 L10,1 C14.9705627,1 19,5.02943725 19,10 L19,10 L19,10 C19,14.9705627 14.9705627,19 10,19 L10,19 L10,19 C5.02943725,19 1,14.9705627 1,10 L1,10 L1,10 C1,5.02943725 5.02943725,1 10,1 L10,1 Z" class="outer"></path>
                </svg>
                <span>Option Two</span>
            </label></div>
            <div class="col-lg-4">
            <label for="rdo-3" class="btn-radio">
                <input type="radio" id="rdo-3" name="radio-grp">
                <svg width="20px" height="20px" viewBox="0 0 20 20">
                    <circle cx="10" cy="10" r="9"></circle>
                    <path d="M10,7 C8.34314575,7 7,8.34314575 7,10 C7,11.6568542 8.34314575,13 10,13 C11.6568542,13 13,11.6568542 13,10 C13,8.34314575 11.6568542,7 10,7 Z" class="inner"></path>
                    <path d="M10,1 L10,1 L10,1 C14.9705627,1 19,5.02943725 19,10 L19,10 L19,10 C19,14.9705627 14.9705627,19 10,19 L10,19 L10,19 C5.02943725,19 1,14.9705627 1,10 L1,10 L1,10 C1,5.02943725 5.02943725,1 10,1 L10,1 Z" class="outer"></path>
                </svg>
                <span>Option Three</span>
            </label></div>
        </div>
    </div>
    <div class="row fill-box" style="height: 200px; background: #BADA55;">
        <h3>Custom 6</h3>
        <div class="custom__6">
            <div class="continput">
                <ul>
                    <li>
                        <input checked type="radio" name="1">
                        <label>OMG a radio!</label>
                        <div class="bullet">
                            <div class="line zero"></div>
                            <div class="line one"></div>
                            <div class="line two"></div>
                            <div class="line three"></div>
                            <div class="line four"></div>
                            <div class="line five"></div>
                            <div class="line six"></div>
                            <div class="line seven"></div>
                        </div>
                    </li>
                    <li>
                        <input type="radio" name="1">
                        <label>Uuuuh radio</label>
                        <div class="bullet">
                            <div class="line zero"></div>
                            <div class="line one"></div>
                            <div class="line two"></div>
                            <div class="line three"></div>
                            <div class="line four"></div>
                            <div class="line five"></div>
                            <div class="line six"></div>
                            <div class="line seven"></div>
                        </div>
                    </li>
                    <li>
                        <input type="radio" name="1">
                        <label>radio everywhere</label>
                        <div class="bullet">
                            <div class="line zero"></div>
                            <div class="line one"></div>
                            <div class="line two"></div>
                            <div class="line three"></div>
                            <div class="line four"></div>
                            <div class="line five"></div>
                            <div class="line six"></div>
                            <div class="line seven"></div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="row fill-box" style="height: 200px; background: linear-gradient(135deg, #AF4665 0%,#D15C73 100%);">
        <h3>Custom 7</h3>
        <div class="custom__7">
            <label>
                <input type="checkbox" />
                <div class="circle">
                    <div class="circle--inner circle--inner__1" ></div>
                    <div class="circle--inner circle--inner__2" ></div>
                    <div class="circle--inner circle--inner__3" ></div>
                    <div class="circle--inner circle--inner__4" ></div>
                    <div class="circle--inner circle--inner__5" ></div>
                    <div class="circle--outer" ></div>
                </div>
                <svg>
                    <defs>
                        <filter id="gooey">
                            <feGaussianBlur in="SourceGraphic" result="blur" stdDeviation="3" />
                            <feColorMatrix in="blur" mode="matrix"
                            values="
                            1 0 0 0 0
                            0 1 0 0 0
                            0 0 1 0 0
                            0 0 0 18 -7
                            " result="gooey" />
                            <feBlend in2="gooey" in="SourceGraphic" result="mix" />
                        </filter>
                    </defs>
                </svg>
            </label>
        </div>
    </div>
    <div class="row fill-box">
        <h3>Custom 5</h3>
        <div></div>
    </div>
    <div class="row fill-box">
        <h3>Custom 5</h3>
        <div></div>
    </div>
</section>

@stop
