@extends("template.layout.".env("APP_LAYOUT", "default"))

@section('head')
<link href="/css/page/component.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
<link href="/css/component/list.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
@endsection

@section('content')
<div class='row'>
    <div class='col-12 col-lg-6 col-xl-4'>
        <div class='list-section'>
            <h4 class='list-title'> Normal List </h4>
            <ul class='normal-list'>
                <li> <i class='icon-red ti-control-record'> </i> Item item </li>
                <li> <i class='ti-control-record'> </i> Item item </li>
                <li> <i class='ti-control-record'> </i> Item item </li>
                <li> <i class='ti-control-record'> </i> Item item </li>
                <li> <i class='ti-control-record'> </i> Item item </li>
            </ul>
        </div>
    </div>
    <div class='col-12 col-lg-6 col-xl-4'>
        <div class='list-section'>
            <h4 class='list-title'> Animated List </h4>
            <ul class='animated-list'>
                <li> Item item </li>
                <li> Item item </li>
                <li> Item item </li>
                <li> Item item </li>
                <li> Item item </li>
            </ul>
        </div>
    </div>
    <div class='col-12 col-lg-6 col-xl-4'>
        <div class='list-section'>
            <h4 class='list-title'> Border List </h4>
            <ul class='border-list'>
                <li> Item item </li>
                <li> Item item </li>
                <li> Item item </li>
                <li> Item item </li>
                <li> Item item </li>
            </ul>
        </div>
    </div>
</div>
@stop
