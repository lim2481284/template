@extends("template.layout.".env("APP_LAYOUT", "default"))

@section('head')
<link href="/css/dashboard/component.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
<link href="/css/dashboard/component/widget.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
<link href="/css/dashboard/component/card.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
<link href="/css/dashboard/component/button.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
<link href="/css/dashboard/component/modal.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
<link href="/css/dashboard/component/list.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>

<script type="text/javascript" src="/js/dashboard/component/widget.js{{ config('app.link_version') }}"></script>
@endsection

@section('content')
<div class='widget-section row'>

</div>
<div class='widget-section row'>
    <h3 class='title'> Clean Widget </h3>
</div>
@stop
