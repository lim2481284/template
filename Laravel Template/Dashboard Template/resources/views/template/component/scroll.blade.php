@extends("template.layout.".env("APP_LAYOUT", "default"))

@section('head')

<link href="/css/page/component.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
<link href="/css/component/scroll.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>

@endsection

@section('content')

<section>
    <div class="text-center" style="height: 2000px; font-size: 50px">
        <h1>Scroll Down</h1>
    </div>
    <div>
        <button onclick="topFunction()" id="myBtn" title="Go to top" class="myBtn">Top</button>
    </div>
    <div>

        <!-- Return to Top -->
        <a href="javascript:" id="return-to-top"><i class="icon-chevron-up"></i></a>

        <!-- ICON NEEDS FONT AWESOME FOR CHEVRON UP ICON -->
        <link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">

    </div>
</section>
<script type="text/javascript">
    // When the user scrolls down 20px from the top of the document, show the button
    window.onscroll = function() {scrollFunction()};

    function scrollFunction() {
        if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
            document.getElementById("myBtn").style.display = "block";
        } else {
            document.getElementById("myBtn").style.display = "none";
        }
    }

    // When the user clicks on the button, scroll to the top of the document
    function topFunction() {
        document.body.scrollTop = 0;
        document.documentElement.scrollTop = 0;
    }

    // ===== Scroll to Top ==== 
    $(window).scroll(function() {
        if ($(this).scrollTop() >= 20) {        // If page is scrolled more than 50px
            $('#return-to-top').fadeIn(200);    // Fade in the arrow
        } else {
            $('#return-to-top').fadeOut(200);   // Else fade out the arrow
        }
    });
    $('#return-to-top').click(function() {      // When arrow is clicked
        $('body,html').animate({
            scrollTop : 0                       // Scroll to top of body
        }, 500);
    });
</script>

@stop
