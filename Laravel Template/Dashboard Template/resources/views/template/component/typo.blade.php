@extends("template.layout.".env("APP_LAYOUT", "default"))

@section('head')
<link href="/css/page/component.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
<link rel="stylesheet" type="text/css" href="/css/component/typo.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/prefixfree/1.0.7/prefixfree.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js" type="text/javascript"></script>

@endsection

@section('content')

<section style="background: #F1F2F7;">
    <div class="banner-title row">
        <h3 class="cop" style="font-size: 100px; margin-top: 0px;">Typography</h3>
    </div>
    <div class="row banner">
        <h3>3D Text </h3>
        <div class="stage">
            <div class="layer"></div>
            <div class="layer"></div>
            <div class="layer"></div>
            <div class="layer"></div>
            <div class="layer"></div>
            <div class="layer"></div>
            <div class="layer"></div>
            <div class="layer"></div>
            <div class="layer"></div>
            <div class="layer"></div>
            <div class="layer"></div>
            <div class="layer"></div>
            <div class="layer"></div>
            <div class="layer"></div>
            <div class="layer"></div>
            <div class="layer"></div>
            <div class="layer"></div>
            <div class="layer"></div>
            <div class="layer"></div>
            <div class="layer"></div>
        </div>
    </div>
    <div class="row banner" style="background: #02111B;">
        <h3 style="color: white;">3D Text Hover</h3>
        <div class="logo">
            <a href="#">Shark<br>Was Here</a>
        </div>
    </div>
    <div class="row banner" style="background: black;height: 1600px;">
        <h3 style="color: white;">80's Typography</h3>
        <div id="wrapper">
            <h1 class="chrome">CHROME</h1>
            <h3 class="dreams">Shark</h3>
            <h1 class="vectro">
                <span class="vectro-body">SHARK</span>
                <span class="vectro-red">I</span>
                <span class="vectro-green">I</span>
                <span class="vectro-blue">I</span>
            </h1>
            <div class="street-machine">
                <h1 class="street">SHARK</h1>
                <h3 class="machine">Machine</h3>
            </div>
            <h1 class="victory"><span class="victory-v">V</span>ictory</h1>
            <div class="future-cop">
                <h3 class="future">Shark</h3>
                <h1 class="cop">COP</h1>
            </div>
        </div>
    </div>
    <div class="row banner">
        <h3>Vintage Title</h3>
        <div class=" bg-vintage">
            <section class="vintage-text">
                <h3 class="vintage">Hey everyone I am Shark</h3>
                <h1 class="vintage"><span class="fontawesome-star star"></span> <span>Awesome Shark Attack</span> <span class="fontawesome-star star"></span></h1>
            </section>
        </div>
    </div>
    <div class="row banner" style="height: 730px;">
        <h3>Shadow Fest</h3>
        <div class="shadow">
            <h1 class='elegantshadow'>Elegant Shadow</h1>
            <h1 class='deepshadow'>Deep Shadow</h1>
            <h1 class='insetshadow'>Inset Shadow</h1>
            <h1 class='retroshadow'>Retro Shadow</h1>
        </div>
    </div>
    <div class="row banner" style="height: 1000px;">
        <h3>Typography</h3>
        <div class="dash-shadow">
            <span data-text="Hello!" class="dashed-shadow hello">Hello!</span><br />
            <div data-text="This thing is" class="dashed-shadow">This thing is</div> <br />
            <div class="sorta-block">
                <div data-text="sort of"class="dashed-shadow sorta">sort of</div> 
            </div>
            <div data-text="a shark-ish" class="dashed-shadow hipsterish">a shark-ish</div> <br />
            <div class="dashed-shadow dashed-shadow-text">DASHED<br /><span class="shadow">SHADOW</span></div> <br />
        </div>
    </div>
    <div class="row banner">
        <h3>Typography</h3>
        <div></div>
    </div>
</section>

<script  src="/js/component/80typo.js"></script>
<script  src="/js/component/dashed-shadow.js"></script>

@stop
