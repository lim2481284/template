@extends("template.layout.".env("APP_LAYOUT", "default"))

@section('head')
<link href="/css/page/component.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
<link href="/css/component/widget.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
<script type="text/javascript" src="/js/component/widget.js{{ config('app.link_version') }}"></script>
@endsection

@section('content')
<div class='widget-section row'>
    <h3 class='title'> Gradient Widget </h3>
    <div class="col-sm-6 col-lg-6 col-xl-3">
        <a href='/dashboard/transaction?filter=all' class='dashboard-link'>
            <div class='dashboard-panel panel-red col-sm-12'>
                <div class='col-sm-12 col-xs-12'>
                    <p class='title'>
                        Panel
                    </p>
                </div>
                <div class='col-sm-12 col-xs-12 panel-right-mobile'>
                    <div class='col-sm-8 col-xs-8'>
                            <label class='panel-number'> 1</label>
                    </div>
                    <div class='col-sm-4 col-xs-4 panel-right-mobile'>
                        <div class='panel-icon-section '>
                            <i class="menu-icon icon-white ti-time"></i>
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div>
    <div class="col-sm-6 col-lg-6 col-xl-3">
        <a href='/dashboard/transaction?filter=all' class='dashboard-link'>
            <div class='dashboard-panel panel-orange col-sm-12'>
                <div class='col-sm-12 col-xs-12'>
                    <p class='title'>
                        Panel
                    </p>
                </div>
                <div class='col-sm-12 col-xs-12 panel-right-mobile'>
                    <div class='col-sm-8 col-xs-8'>
                            <label class='panel-number'> 1</label>
                    </div>
                    <div class='col-sm-4 col-xs-4 panel-right-mobile'>
                        <div class='panel-icon-section '>
                            <i class="menu-icon icon-white ti-time"></i>
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div>
    <div class="col-sm-6 col-lg-6 col-xl-3">
        <a href='/dashboard/transaction?filter=0' class='dashboard-link'>
            <div class='dashboard-panel panel-blue col-sm-12'>
                <div class='col-sm-12 col-xs-12'>
                    <p class='title'>
                        Panel 1
                    </p>
                </div>
                <div class='col-sm-12 col-xs-12 panel-right-mobile'>
                    <div class='col-sm-8 col-xs-8'>
                            <label class='panel-number'>2</label>
                    </div>
                    <div class='col-sm-4 col-xs-4 panel-right-mobile'>
                        <div class='panel-icon-section '>
                            <i class="menu-icon icon-white ti-control-record"></i>
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div>

    <div class="col-sm-6 col-lg-6 col-xl-3">
        <a href='/dashboard/transaction?filter=1' class='dashboard-link'>
            <div class='dashboard-panel panel-purple col-sm-12'>
                <div class='col-sm-12 col-xs-12'>
                    <p class='title'>
                        Panel 2
                    </p>

                </div>
                <div class='col-sm-12 col-xs-12 panel-right-mobile'>
                    <div class='col-sm-8 col-xs-8'>
                            <label class='panel-number'>3</label>
                    </div>
                    <div class='col-sm-4 col-xs-4 panel-right-mobile'>
                        <div class='panel-icon-section '>
                            <i class="menu-icon icon-white ti-face-smile"></i>
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div>
    <div class="col-sm-6 col-lg-6 col-xl-3">
        <a href='/dashboard/transaction?filter=2' class='dashboard-link'>
            <div class='dashboard-panel panel-green col-sm-12'>
                <div class='col-sm-12 col-xs-12'>
                    <p class='title'>
                        Panel 3
                    </p>
                </div>
                <div class='col-sm-12 col-xs-12 panel-right-mobile'>
                    <div class='col-sm-8 col-xs-8'>
                            <label class='panel-number'>4</label>
                    </div>
                    <div class='col-sm-4 col-xs-4 panel-right-mobile'>
                        <div class='panel-icon-section '>
                            <i class="menu-icon icon-white ti-check"></i>
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div>
    <div class="col-sm-6 col-lg-6 col-xl-3">
        <a href='/dashboard/transaction?filter=all' class='dashboard-link'>
            <div class='dashboard-panel panel-dark col-sm-12'>
                <div class='col-sm-12 col-xs-12'>
                    <p class='title'>
                        Panel
                    </p>
                </div>
                <div class='col-sm-12 col-xs-12 panel-right-mobile'>
                    <div class='col-sm-8 col-xs-8'>
                            <label class='panel-number'> 1</label>
                    </div>
                    <div class='col-sm-4 col-xs-4 panel-right-mobile'>
                        <div class='panel-icon-section '>
                            <i class="menu-icon icon-white ti-time"></i>
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div>
</div>
<div class='widget-section row'>
    <h3 class='title'> Clean Widget </h3>
    <div class="col-sm-6 col-lg-6 col-xl-3">
        <a href='/dashboard/transaction?filter=all' class='dashboard-link'>
            <div class='dashboard-panel clean-panel col-sm-12'>
                <div class='col-4 left-section'>
                    <i class="menu-icon icon-red ti-time"></i>
                </div>
                <div class='col-8 '>
                    <label class='title'> Title </label>
                    <label class='subtitle'> 1</label>
                </div>
            </div>
        </a>
    </div>
    <div class="col-sm-6 col-lg-6 col-xl-3">
        <a href='/dashboard/transaction?filter=all' class='dashboard-link'>
            <div class='dashboard-panel clean-panel col-sm-12'>
                <div class='col-4 left-section'>
                    <i class="menu-icon icon-blue ti-time"></i>
                </div>
                <div class='col-8 '>
                    <label class='title'> Title </label>
                    <label class='subtitle'> 1</label>
                </div>
            </div>
        </a>
    </div>
    <div class="col-sm-6 col-lg-6 col-xl-3">
        <a href='/dashboard/transaction?filter=all' class='dashboard-link'>
            <div class='dashboard-panel clean-panel col-sm-12'>
                <div class='col-4 left-section'>
                    <i class="menu-icon icon-green ti-time"></i>
                </div>
                <div class='col-8 '>
                    <label class='title'> Title </label>
                    <label class='subtitle'> 1</label>
                </div>
            </div>
        </a>
    </div>
    <div class="col-sm-6 col-lg-6 col-xl-3">
        <a href='/dashboard/transaction?filter=all' class='dashboard-link'>
            <div class='dashboard-panel clean-panel col-sm-12'>
                <div class='col-4 left-section'>
                    <i class="menu-icon icon-purple ti-time"></i>
                </div>
                <div class='col-8 '>
                    <label class='title'> Title </label>
                    <label class='subtitle'> 1</label>
                </div>
            </div>
        </a>
    </div>
    <div class="col-sm-6 col-lg-6 col-xl-3">
        <a href='/dashboard/transaction?filter=all' class='dashboard-link'>
            <div class='dashboard-panel clean-panel col-sm-12'>
                <div class='col-4 left-section'>
                    <i class="menu-icon icon-lightred ti-time"></i>
                </div>
                <div class='col-8 '>
                    <label class='title'> Title </label>
                    <label class='subtitle'> 1</label>
                </div>
            </div>
        </a>
    </div>
    <div class="col-sm-6 col-lg-6 col-xl-3">
        <a href='/dashboard/transaction?filter=all' class='dashboard-link'>
            <div class='dashboard-panel clean-panel col-sm-12'>
                <div class='col-4 left-section'>
                    <i class="menu-icon icon-lightpurple ti-time"></i>
                </div>
                <div class='col-8 '>
                    <label class='title'> Title </label>
                    <label class='subtitle'> 1</label>
                </div>
            </div>
        </a>
    </div>
    <div class="col-sm-6 col-lg-6 col-xl-3">
        <a href='/dashboard/transaction?filter=all' class='dashboard-link'>
            <div class='dashboard-panel clean-panel col-sm-12'>
                <div class='col-4 left-section'>
                    <i class="menu-icon icon-lightblue ti-time"></i>
                </div>
                <div class='col-8 '>
                    <label class='title'> Title </label>
                    <label class='subtitle'> 1</label>
                </div>
            </div>
        </a>
    </div>
</div>
@stop
