@extends("template.layout.".env("APP_LAYOUT", "default"))

@section('head')
   <link href="/css/plugin/froala_codemirror.min.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
   <link href="/css/plugin/froala_editor.min.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
   <link href="/css/plugin/froala_style.min.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
   <link href="/css/component/form.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>

   <script type="text/javascript" src="/js/plugin/froala_codemirror.min.js{{ config('app.link_version') }}"></script>
   <script type="text/javascript" src="/js/plugin/froala_xml.min.js{{ config('app.link_version') }}"></script>
   <script type="text/javascript" src="/js/plugin/froala_editor.min.js{{ config('app.link_version') }}"></script>
   <script type="text/javascript" src="/js/component/form.js{{ config('app.link_version') }}"></script>
@endsection

@section('content')

<div class='form-box'>
    <i class='ti-pencil-alt icon-blue'> </i>
    <h3 class='title'>Text Editor </h3>

    <div class="form-group row text-editor-container">
        <div id="froala-editor">
          <p><strong>Dummy text to enable scroll.</strong></p>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean ornare lorem ut pellentesque tempor. Vivamus ut ex vestibulum velit rich text editor eleifend fringilla. Sed non metus dictum, elementum mauris wysiwyg html editor non, sagittis odio. Nullam pellentesque leo sit amet ante suscipit wysiwyg html editor sagittis. Donec tempus vulputate suscipit. Ut non felis rich text editor ac dolor pulvinar lacinia eu eget urna. Sed tincidunt sapien vulputate tellus fringilla sodales. Morbi accumsan dui wysiwyg html editor sed massa pellentesque, quis vestibulum lectus scelerisque. Nulla ultrices mi id felis luctus aliquet. Donec nec ligula wysiwyg html editor pretium sapien semper dictum eu id quam. Etiam ut sollicitudin nibh. Quisque eu ultrices dui. Nunc rich text editor congue, enim vitae dictum dignissim, libero nisl sagittis augue, non aliquet nibh tortor sit amet ex. Aliquam cursus maximus rich text editor mi eu consequat. Nullam tincidunt erat et placerat mattis. Nunc rich text editor congue, enim vitae dictum dignissim, libero nisl sagittis augue, non aliquet nibh tortor sit amet ex. Aliquam cursus maximus mi eu consequat. Nullam tincidunt erat et placerat mattis.</p>
        </div>
    </div>
    <button class='btn btn-success'> Save </button>
    <button class='btn btn-default'> Cancel </button>
</div>
@stop
