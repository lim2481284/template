@extends("template.layout.".env("APP_LAYOUT", "default"))

@section('head')
<link href="/css/component/form.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
<script type="text/javascript" src="/js/component/form.js{{ config('app.link_version') }}"></script>
@endsection

@section('content')
<div class='row'>
    <div class="col-sm-6 col-lg-6 col-xl-4">
        <div class='form-box'>
            <i class='ti-book icon-red'> </i>
            <h3 class='title'>Form Title</h3>

            <div class="form-group row">
                <label for="example-week-input" class="col-12 col-sm-2 col-form-label">Week</label>
                <div class="col-12 col-sm-10">
                    <input class="form-control" type="week" value="2011-W33" id="example-week-input">
                </div>
            </div>
            <div class="form-group row">
                <label for="example-time-input" class="col-12 col-sm-2 col-form-label">Time</label>
                <div class="col-12 col-sm-10">
                    <input class="form-control" type="time" value="13:45:00" id="example-time-input">
                </div>
            </div>
            <div class="form-group row">
                <label for="example-color-input" class="col-12 col-sm-2 col-form-label">Color</label>
                <div class="col-12 col-sm-10">
                    <input class="form-control" type="color" value="#563d7c" id="example-color-input">
                </div>
            </div>
            <button class='btn btn-primary'> Submit </button>
            <button class='btn btn-default'> Cancel </button>
        </div>
    </div>

    <div class="col-sm-6 col-lg-6 col-xl-4">
        <div class='form-box'>
            <i class='ti-book icon-red'> </i>
            <h3 class='title'>Form Title</h3>

            <div class="form-group row">
                <label for="example-week-input" class="col-12 col-sm-2 col-form-label">Week</label>
                <div class="col-12 col-sm-10">
                    <input class="form-control" type="week" value="2011-W33" id="example-week-input">
                </div>
            </div>
            <div class="form-group row">
                <label for="example-time-input" class="col-12 col-sm-2 col-form-label">Time</label>
                <div class="col-12 col-sm-10">
                    <input class="form-control" type="time" value="13:45:00" id="example-time-input">
                </div>
            </div>
            <div class="form-group row">
                <label for="example-color-input" class="col-12 col-sm-2 col-form-label">Color</label>
                <div class="col-12 col-sm-10">
                    <input class="form-control" type="color" value="#563d7c" id="example-color-input">
                </div>
            </div>
            <button class='btn btn-primary'> Submit </button>
            <button class='btn btn-default'> Cancel </button>
        </div>
    </div>

    <div class="col-sm-6 col-lg-6 col-xl-4">
        <div class='form-box'>
            <i class='ti-book icon-red'> </i>
            <h3 class='title'>Form Title</h3>

            <div class="form-group row">
                <label for="example-week-input" class="col-12 col-sm-2 col-form-label">Week</label>
                <div class="col-12 col-sm-10">
                    <input class="form-control" type="week" value="2011-W33" id="example-week-input">
                </div>
            </div>
            <div class="form-group row">
                <label for="example-time-input" class="col-12 col-sm-2 col-form-label">Time</label>
                <div class="col-12 col-sm-10">
                    <input class="form-control" type="time" value="13:45:00" id="example-time-input">
                </div>
            </div>
            <div class="form-group row">
                <label for="example-color-input" class="col-12 col-sm-2 col-form-label">Color</label>
                <div class="col-12 col-sm-10">
                    <input class="form-control" type="color" value="#563d7c" id="example-color-input">
                </div>
            </div>
            <button class='btn btn-primary'> Submit </button>
            <button class='btn btn-default'> Cancel </button>
        </div>
    </div>
</div>

<div class='form-box'>
    <i class='ti-info-alt icon-blue'> </i>
    <h3 class='title'>Form Title</h3>
    <div class="form-group row">
        <label for="example-text-input" class="col-12 col-sm-2 col-form-label">Text</label>
        <div class="col-12 col-sm-10">
            <input class="form-control" type="text" value="Artisanal kale" id="example-text-input">
        </div>
    </div>
    <div class="form-group row">
        <label for="example-search-input" class="col-12 col-sm-2 col-form-label">Search</label>
        <div class="col-12 col-sm-10">
            <input class="form-control" type="search" value="How do I shoot web" id="example-search-input">
        </div>
    </div>
    <div class="form-group row">
        <label for="example-email-input" class="col-12 col-sm-2 col-form-label">Email</label>
        <div class="col-12 col-sm-10">
            <input class="form-control" type="email" value="bootstrap@example.com" id="example-email-input">
        </div>
    </div>
    <div class="form-group row">
        <label for="example-url-input" class="col-12 col-sm-2 col-form-label">URL</label>
        <div class="col-12 col-sm-10">
            <input class="form-control" type="url" value="https://getbootstrap.com" id="example-url-input">
        </div>
    </div>
    <div class="form-group row">
        <label for="example-tel-input" class="col-12 col-sm-2 col-form-label">Telephone</label>
        <div class="col-12 col-sm-10">
            <input class="form-control" type="tel" value="1-(555)-555-5555" id="example-tel-input">
        </div>
    </div>
    <div class="form-group row">
        <label for="example-password-input" class="col-12 col-sm-2 col-form-label">Password</label>
        <div class="col-12 col-sm-10">
            <input class="form-control" type="password" value="hunter2" id="example-password-input">
        </div>
    </div>
    <div class="form-group row">
        <label for="example-number-input" class="col-12 col-sm-2 col-form-label">Number</label>
        <div class="col-12 col-sm-10">
            <input class="form-control" type="number" value="42" id="example-number-input">
        </div>
    </div>
    <div class="form-group row">
        <label for="example-datetime-local-input" class="col-12 col-sm-2 col-form-label">Date and time</label>
        <div class="col-12 col-sm-10">
            <input class="form-control" type="datetime-local" value="2011-08-19T13:45:00" id="example-datetime-local-input">
        </div>
    </div>
    <div class="form-group row">
        <label for="example-date-input" class="col-12 col-sm-2 col-form-label">Date</label>
        <div class="col-12 col-sm-10">
            <input class="form-control" type="date" value="2011-08-19" id="example-date-input">
        </div>
    </div>
    <div class="form-group row">
        <label for="example-month-input" class="col-12 col-sm-2 col-form-label">Month</label>
        <div class="col-12 col-sm-10">
            <input class="form-control" type="month" value="2011-08" id="example-month-input">
        </div>
    </div>
    <div class="form-group row">
        <label for="example-week-input" class="col-12 col-sm-2 col-form-label">Week</label>
        <div class="col-12 col-sm-10">
            <input class="form-control" type="week" value="2011-W33" id="example-week-input">
        </div>
    </div>
    <div class="form-group row">
        <label for="example-time-input" class="col-12 col-sm-2 col-form-label">Time</label>
        <div class="col-12 col-sm-10">
            <input class="form-control" type="time" value="13:45:00" id="example-time-input">
        </div>
    </div>
    <div class="form-group row">
        <label for="example-color-input" class="col-12 col-sm-2 col-form-label">Color</label>
        <div class="col-12 col-sm-10">
            <input class="form-control" type="color" value="#563d7c" id="example-color-input">
        </div>
    </div>
</div>

<div class='form-box'>
    <i class='ti-book icon-red'> </i>
    <h3 class='title'>Form Title</h3>

    <div class="form-group row">
        <label for="example-week-input" class="col-12 col-sm-2 col-form-label">Week</label>
        <div class="col-12 col-sm-10">
            <input class="form-control" type="week" value="2011-W33" id="example-week-input">
        </div>
    </div>
    <div class="form-group row">
        <label for="example-time-input" class="col-12 col-sm-2 col-form-label">Time</label>
        <div class="col-12 col-sm-10">
            <input class="form-control" type="time" value="13:45:00" id="example-time-input">
        </div>
    </div>
    <div class="form-group row">
        <label for="example-color-input" class="col-12 col-sm-2 col-form-label">Color</label>
        <div class="col-12 col-sm-10">
            <input class="form-control" type="color" value="#563d7c" id="example-color-input">
        </div>
    </div>
    <button class='btn btn-primary'> Submit </button>
    <button class='btn btn-default'> Cancel </button>
</div>
@stop
