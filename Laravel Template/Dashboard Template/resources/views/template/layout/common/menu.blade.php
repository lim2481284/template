<ul class="nav navbar-nav">
    <li class="{{ ((Request::route()->getName()=='template.index')?'active':' ')}}">
        <a href='/'>
            <i class="menu-icon icon-red ti-home"></i>
            Dashboard
        </a>
    </li>
    <li class="menu-item-has-children dropdown {{ ((Request::route()->getName()=='template.component')?'active':' ')}}">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon icon-blue fa fa-laptop"></i>Components</a>
        <ul class="sub-menu children dropdown-menu">
            <li><i class="ti-image"></i><a href="{{Route('template.component.widget')}}">Widget</a></li>
            <li><i class="fa fa-puzzle-piece"></i><a href="{{Route('template.component.button')}}">Button</a></li>
            <li><i class="ti-target"></i><a href="{{Route('template.component.tab')}}">Tab</a></li>
            <li><i class="ti-marker"></i><a href="{{Route('template.component.list')}}">List</a></li>
            <li><i class="ti-layout"></i><a href="{{Route('template.component.card')}}">Card</a></li>
            <li><i class="ti-heart"></i><a href="{{Route('template.component.typo')}}">Typography</a></li>
            <li><i class="ti-flag"></i><a href="{{Route('template.component.modal')}}">Modal</a></li>
            <li><i class="ti-target"></i><a href="{{Route('template.component.widget')}}">Panel Section</a></li>
            <li><i class="fa fa-puzzle-piece"></i><a href="{{Route('template.component.input')}}">Input</a></li>
            <li><i class="fa fa-puzzle-piece"></i><a href="{{Route('template.component.scroll')}}">Scroll Top Button</a></li>
            <li><i class="ti-marker"></i><a href="{{Route('template.component.widget')}}">Section Layout</a></li>
            <li><i class="ti-layout"></i><a href="{{Route('template.component.widget')}}">Loader</a></li>
        </ul>
    </li>
    <li class="menu-item-has-children dropdown {{ ((Request::route()->getName()=='template.table')?'active':' ')}}">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon icon-purple ti-notepad"></i>Tables</a>
        <ul class="sub-menu children dropdown-menu">
            <li><i class="ti-image"></i><a href="{{Route('template.table.basic')}}">Basic Table</a></li>
            <li><i class="fa fa-puzzle-piece"></i><a href="{{Route('template.table.datatable.search')}}">Data Table</a></li>
            <li><i class="ti-target"></i><a href="{{Route('template.table.form')}}">Form Table</a></li>
            <li><i class="ti-target"></i><a href="{{Route('template.table.form2')}}">Combined Form Table </a></li>
        </ul>
    </li>
    <li class="menu-item-has-children dropdown {{ ((Request::route()->getName()=='template.form')?'active':' ')}}">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon icon-green ti-file"></i>Form</a>
        <ul class="sub-menu children dropdown-menu">
            <li><i class="ti-image"></i><a href="{{Route('template.form.basic')}}">Basic Form</a></li>
            <li><i class="fa fa-puzzle-piece"></i><a href="{{Route('template.form.advanced')}}">Advanced Form</a></li>
            <li><i class="fa fa-puzzle-piece"></i><a href="{{Route('template.form.advanced')}}">Wallet Form</a></li>
            <li><i class="ti-target"></i><a href="{{Route('template.form.advanced')}}">Profile Form</a></li>
            <li><i class="ti-target"></i><a href="{{Route('template.form.advanced')}}">Contact Form</a></li>
            <li><i class="ti-target"></i><a href="{{Route('template.form.advanced')}}">Login Form</a></li>
            <li><i class="ti-image"></i><a href="{{Route('template.form.advanced')}}">Multiple Step Form</a></li>
        </ul>
    </li>
    <li class="menu-item-has-children dropdown {{ ((Request::route()->getName()=='template.table')?'active':' ')}}">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon icon-orange ti-layers-alt"></i>Animation & Effect </a>
        <ul class="sub-menu children dropdown-menu">
            <li><i class="ti-image"></i><a href="{{Route('template.form.advanced')}}">Custom</a></li>
            <li><i class="ti-image"></i><a href="{{Route('template.form.advanced')}}">3D</a></li>
            <li><i class="ti-image"></i><a href="{{Route('template.form.advanced')}}">Background</a></li>
        </ul>
    </li>
    <li class="menu-item-has-children dropdown {{ ((Request::route()->getName()=='template.table')?'active':' ')}}">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon icon-lightred ti-layers-alt"></i>Plugins</a>
        <ul class="sub-menu children dropdown-menu">
            <li><i class="ti-image"></i><a href="{{Route('template.plugin.chart')}}">Transition </a></li>
            <li><i class="ti-image"></i><a href="{{Route('template.plugin.carousel')}}">Carousel</a></li>
            <li><i class="ti-heart"></i><a href="{{Route('template.plugin.icons')}}">Icon</a></li>
            <li><i class="ti-marker"></i><a href="{{Route('template.plugin.chart')}}">Tour</a></li>
            <li><i class="ti-target"></i><a href="{{Route('template.plugin.popup')}}">Popup</a></li>
            <li><i class="ti-image"></i><a href="{{Route('template.plugin.chart')}}">Timeline</a></li>
            <li><i class="ti-image"></i><a href="{{Route('template.plugin.chart')}}">Calendar</a></li>
            <li><i class="ti-image"></i><a href="{{Route('template.plugin.chart')}}">Chat</a></li>
            <li><i class="ti-image"></i><a href="{{Route('template.plugin.chart')}}">Chart</a></li>
            <li><i class="ti-image"></i><a href="{{Route('template.plugin.map')}}">Map</a></li>
            <li><i class="ti-image"></i><a href="{{Route('template.plugin.tree')}}">Tree</a></li>
            <li><i class="ti-image"></i><a href="{{Route('template.plugin.chart')}}">Floating Chat Bot</a></li>
        </ul>
    </li>
    <hr>
    <li class="{{ ((Request::route()->getName()=='template.feedback')?'active':' ')}}">
        <a href='/template/feedback'>
            <i class="menu-icon ti-shift-left"></i>
            Logout
        </a>
    </li>
</ul>
