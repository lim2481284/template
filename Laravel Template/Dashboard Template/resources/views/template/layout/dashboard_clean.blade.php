<!doctype html>
<html lang="{{ app()->getLocale() }}">

<head>

    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>{{env("APP_NAME")}}</title>
    <!-- Meta End-->

    <!-- Style setting -->
    @include('template.setting.theme')
    <!-- Style setting end -->

    <!-- Styles -->
    <link rel="shortcut icon" href='/img/logo/{{env("APP_LOGO", "logo")}}.ico'/>
    <link href="/css/plugin/bootstrap.min.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
    <link href="/css/plugin/normalize.min.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
    <link href="/css/plugin/fontawesome.min.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
    <link href="/css/plugin/themify-icons.min.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="/css/plugin/open-iconic.min.css">
    <link rel="stylesheet" type="text/css" href="/css/plugin/tooltipster.bundle.min.css" />
    <link href="/css/page/foundation.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
    <link href="/css/theme/{{env('APP_LAYOUT')}}.min.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
    <link href="/css/theme/{{env('APP_LAYOUT')}}_responsive.min.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
    <link href="/css/page/responsive.min.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
    <!-- Styles end -->

    <!-- Script -->
    <script type="text/javascript" src="/js/plugin/jquery.min.js{{ config('app.link_version') }}"></script>
    <script type="text/javascript" src="/js/plugin/tooltipster.bundle.min.js{{ config('app.link_version') }}"></script>
    <script type="text/javascript" src="/js/plugin/bootstrap.min.js{{ config('app.link_version') }}"></script>
    <script type="text/javascript" src="/js/plugin/sweetalert.min.js{{ config('app.link_version') }}"></script>
    <script type="text/javascript" src="/js/page/foundation.min.js{{ config('app.link_version') }}"></script>
    <script type="text/javascript" src="/js/theme/{{env('APP_LAYOUT')}}.min.js{{ config('app.link_version') }}"></script>
    <!-- Script End -->

    @yield('head')

</head>

<body>

    <div class='bg-color'></div>

    <!--Loader section -->
    <div class='page-loader'>
        <div class='loader'>
            <img src='/img/icon/{{env("APP_LOGO", "logo")}}_loader.gif'/>
            <label class='small'> Loading </label>
        </div>
    </div>
    <!--Loader section end -->

    <!-- Menu Panel -->
    <aside id="left-panel" class="left-panel">
        <nav class="navbar navbar-expand-sm navbar-default">

            <div class="navbar-header">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand" href="./"><img src='/img/logo/{{env("APP_LOGO", "logo")}}.png' alt="Logo"></a>
                <a class="navbar-brand hidden" href="./"><img src='/img/logo/{{env("APP_LOGO", "logo")}}_small.png'  alt="Logo"></a>
            </div>

            <div id="main-menu" class="main-menu collapse navbar-collapse">
                @include('template.layout.common.menu')
            </div>
        </nav>
    </aside>
    <!-- Menu Panel End-->

    <!-- Content Panel -->
    <div id="right-panel" class="right-panel">

        <!-- Header -->
        <header id="header" class="header">
            <div class="header-menu">

                <!-- Searchbar -->
                <div class="col-sm-6">
                    <a id="menuToggle" class="menutoggle pull-left"><i class="fa fa fa-tasks"></i></a>
                </div>
                <!-- Searchbar End -->

                <div class="col-sm-6 user-col">
                    <!-- Profile -->
                    <div class="user-area dropdown float-right">
                        <div class='col-sm-7'>
                            <div class="input-group mb-3">
                              <div class="input-group-prepend">
                                  <span class="input-group-text white-input-group"><i class='ti-world icon-red'> </i></span>
                              </div>
                              {{ Form::select('localization_language', getSupportedLocalesNative(), LaravelLocalization::getLocalizedURL(LaravelLocalization::getCurrentLocale() , null, [], true)  , ['class' => 'form-control language-select col-centered']) }}
                            </div>
                        </div>
                        <div class='col-sm-5'>
                            <a href="#" class="" aria-haspopup="true" aria-expanded="false" >
                                <img class="user-avatar rounded-circle profile-tooltip" src="/img/icon/profile.png" alt="User Avatar"  data-tooltip-content="#tooltip_content">
                            </a>
                            <div class="tooltip_templates">
                                <span id="tooltip_content">
                                    <a href="/dashboard/logout"> <i class="menu-icon icon-lightblue ti-share"></i>Profile  </a>
                                    <a href="/dashboard/logout"> <i class="menu-icon icon-lightred ti-share"></i>Logout  </a>
                                </span>
                            </div>
                            <label class='username'> Username  </label>
                        </div>
                    </div>
                    <!-- Profile End -->

                    <!-- Language  -->
                    <div class="language-select dropdown" id="language-select">
                        <a class="dropdown-toggle" href="#" data-toggle="dropdown"  id="language" aria-haspopup="true" aria-expanded="true">
                            <i class="flag-icon flag-icon-us"></i>
                        </a>
                        <div class="dropdown-menu" aria-labelledby="language" >
                            <div class="dropdown-item">
                                <span class="flag-icon flag-icon-fr"></span>
                            </div>
                            <div class="dropdown-item">
                                <i class="flag-icon flag-icon-es"></i>
                            </div>
                            <div class="dropdown-item">
                                <i class="flag-icon flag-icon-us"></i>
                            </div>
                            <div class="dropdown-item">
                                <i class="flag-icon flag-icon-it"></i>
                            </div>
                        </div>
                    </div>
                    <!-- Language End -->

                </div>
            </div>
        </header>
        <!-- Header End-->

        <!-- Body Content -->
        <div class="content mt-3">
            @if(env('DASHBOARD_BG_PICTURE'))
                <img class='dashboard-bg' src="/img/picture/{{env('DASHBOARD_BG_PICTURE')}}.png"/>
            @endif
            @yield('content')
        </div>
        <!-- Body Content End-->

    </div>
    <!-- Content Panel End-->

</body>
</html>
