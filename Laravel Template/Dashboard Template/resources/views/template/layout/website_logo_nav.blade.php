<!doctype html>
<html lang="{{ app()->getLocale() }}">

<head>
    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>{{env("APP_NAME")}}</title>
    <!-- Meta End-->

    <!-- Style setting -->
    @include('dashboard/setting.theme')
    <!-- Style setting end -->

    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Raleway" />
    <link rel="shortcut icon" href='/img/logo/{{env("APP_LOGO", "logo")}}.ico'/>
    <link href="/css/plugin/dashboard_bootstrap.min.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
    <link href="/css/plugin/normalize.min.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
    <link href="/css/plugin/fontawesome.min.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
    <link href="/css/plugin/themify-icons.min.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
    <link href="/css/plugin/flatpickr.min.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="/css/plugin/tooltipster.bundle.min.css" />
    <link href="/css/dashboard/main.min.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
    <link href="/css/dashboard/responsive.min.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
    <!-- Styles end -->

    <!-- Script -->
    <script type="text/javascript" src="/js/plugin/dashboard_jquery.min.js{{ config('app.link_version') }}"></script>
    <script type="text/javascript" src="/js/plugin/tooltipster.bundle.min.js{{ config('app.link_version') }}"></script>
    <script type="text/javascript" src="/js/plugin/dashboard_bootstrap.min.js{{ config('app.link_version') }}"></script>
    <script type="text/javascript" src="/js/plugin/sweetalert.min.js{{ config('app.link_version') }}"></script>
    <script type="text/javascript" src="/js/plugin/flatpickr.min.js{{ config('app.link_version') }}"></script>
    <script type="text/javascript" src="/js/dashboard/main.min.js{{ config('app.link_version') }}"></script>
    <!-- Script End -->

    @yield('head')
</head>

<body>
    <div class='bg-color'></div>

    <!--Loader section -->
    <div class='page-loader'>
        <div class='loader'>
            <img src='/img/icon/{{env("APP_LOGO", "logo")}}_loader.gif'/>
            <label class='small'> Loading </label>
        </div>
    </div>
    <!--Loader section end -->

    <!-- Header -->
    <div id="header">
        <div class="container">
            <div>
                <img class="logo" src="images/logo.png">
                <h1>Navbar with logo on top template</h1>
            </div>
            <nav id="nav">
                <ul>
                    <li><a href="index.html">Home</a></li>
                    <li>
                        <a href="">Dropdown</a>
                        <ul>
                            <li><a href="#">Lorem ipsum dolor</a></li>
                            <li><a href="#">Magna phasellus</a></li>
                            <li><a href="#">Etiam dolore nisl</a></li>
                            <li>
                                <a href="">Phasellus consequat</a>
                                <ul>
                                    <li><a href="#">Lorem ipsum dolor</a></li>
                                    <li><a href="#">Phasellus consequat</a></li>
                                    <li><a href="#">Magna phasellus</a></li>
                                    <li><a href="#">Etiam dolore nisl</a></li>
                                    <li><a href="#">Veroeros feugiat</a></li>
                                </ul>
                            </li>
                            <li><a href="#">Veroeros feugiat</a></li>
                        </ul>
                    </li>
                    <li><a href="#">Left Sidebar</a></li>
                    <li><a href="#">Right Sidebar</a></li>
                    <li><a href="#">No Sidebar</a></li>
                </ul>
            </nav>
        </div>
    </div>
    <!-- Header End -->


    <!-- Body Content -->
    <div class="content">
        @yield('content')
    </div>
    <!-- Body Content End-->


    <!-- Footer -->
    <div id="footer">
        <div class="container">
            <div class="row">
                <div class="8u">
                    <section>
                        <header class="major">
                            <h2>Donec dictum metus</h2>
                            <span class="byline">Quisque semper augue mattis wisi maecenas ligula</span>
                        </header>
                        <div class="row">
                            <section class="6u">
                                <ul class="default">
                                    <li><a href="#">Pellentesque elit non gravida blandit.</a></li>
                                    <li><a href="#">Lorem ipsum dolor consectetuer elit.</a></li>
                                    <li><a href="#">Phasellus nibh pellentesque congue.</a></li>
                                    <li><a href="#">Cras vitae metus aliquam  pharetra.</a></li>
                                </ul>
                            </section>
                            <section class="6u">
                                <ul class="default">
                                    <li><a href="#">Pellentesque elit non gravida blandit.</a></li>
                                    <li><a href="#">Lorem ipsum dolor consectetuer elit.</a></li>
                                    <li><a href="#">Phasellus nibh pellentesque congue.</a></li>
                                    <li><a href="#">Cras vitae metus aliquam  pharetra.</a></li>
                                </ul>
                            </section>
                        </div>
                    </section>
                </div>
                <div class="4u">
                    <section>
                        <header class="major">
                            <h2>Get in Touch</h2>
                            <span class="byline">We're good at being 'responsive'</span>
                        </header>
                        <ul class="contact">
                            <li>
                                <span class="address">Address</span>
                                <span>1234 Somewhere Road #4285 <br />Nashville, TN 00000</span>
                            </li>
                            <li>
                                <span class="mail">Mail</span>
                                <span><a href="#">someone@untitled.tld</a></span>
                            </li>
                            <li>
                                <span class="phone">Phone</span>
                                <span>(000) 000-0000</span>
                            </li>
                        </ul>
                    </section>
                </div>
            </div>
            <div class="copyright">
                <p>&copy; <a href="http://fotogrph.com/">Pan Fareast</a> 2018 | All rights reserved.</p>
            </div>
        </div>
    </div>
    <!--Footer end-->


</body>
</html>
