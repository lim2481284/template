
<div id="exportPDFModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            {!! Form::open(['route' => 'template.datatable.exportpdf']) !!}
                <div class="modal-header">
                    <h4 class="modal-title">
                        <i class='ti-share'> </i>
                        Export to PDF
                    </h4>
                </div>
                <div class="modal-body export-modal row">
                    <div class='col-sm-12 form-group'>
                        <p>Start Date </p>
                        <input type="date" class="form-control datepicker" name="startDate">
                    </div>
                    <div class='col-sm-12 form-group'>
                        <p>End Date </p>
                        <input type="date" class="form-control datepicker" name="endDate">
                    </div>
                    <div class='col-sm-12 form-group'>
                        <p>Maximum Records ( Optional, left empty to export all )</p>
                        <input type="number" class="form-control" name="limit">
                    </div>
                </div>
                <div class="modal-footer">
                    {!! Form::submit('Export Now',['class'=>'btn btn-success']) !!}
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

<div id="exportCSVModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            {!! Form::open(['route' => 'template.datatable.exportcsv']) !!}
                <div class="modal-header">
                    <h4 class="modal-title">
                        <i class='ti-share'> </i>
                        Export to CSV
                    </h4>
                </div>
                <div class="modal-body export-modal row">
                    <div class='col-sm-12 form-group'>
                        <p>Start Date </p>
                        <input type="date" class="form-control datepicker" name="startDate">
                    </div>
                    <div class='col-sm-12 form-group'>
                        <p>End Date </p>
                        <input type="date" class="form-control datepicker" name="endDate">
                    </div>
                    <div class='col-sm-12 form-group'>
                        <p>Maximum Records ( Optional, left empty to export all )    </p>
                        <input type="number" class="form-control" name="limit">
                    </div>
                </div>
                <div class="modal-footer">
                    {!! Form::submit('Export Now',['class'=>'btn btn-success']) !!}
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
