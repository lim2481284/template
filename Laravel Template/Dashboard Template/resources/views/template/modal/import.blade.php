
<div id="importModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            {!! Form::open(['route' => 'template.datatable.import.view','files'=>'true', 'id' =>'file-upload-form' ,'class'=>'uploader']) !!}
            <div class="modal-header">
                <h4 class="modal-title"><i class='ti-share-alt'> </i>Import to CSV</h4>
            </div>
            <div class="modal-body export-modal row">
                <div class='col-sm-12 form-group'>
                    <p>Upload CSV </p>
                    <input id="file-upload" type="file" name="csvFile"/>
                    <label for="file-upload" id="file-drag">
                        <img id="file-image" src="#" alt="Preview" class="hidden">
                        <div id="start">
                            <i class="fa fa-download" aria-hidden="true"></i>
                            <div>Select a file or drag here</div>
                            <div id="notimage" class="hidden">Please select an image</div>
                            <span id="file-upload-btn" class="uploader-btn btn btn-primary">Select a file</span>
                        </div>
                        <div id="response" class="hidden">
                            <div id="messages"></div>
                            <progress class="progress" id="file-progress" value="0">
                                <span>0</span>%
                            </progress>
                        </div>
                    </label>
                </div>
                <div class='col-sm-12 form-group'>
                    <p>Sample CSV </p>
                    <a href="{{url('/').'/sample/sample_user_excel.csv'}}" download>
                        <button type='button' class='btn btn-default'> Download </button>
                    </a>
                </div>
            </div>
            <div class="modal-footer">
                {!! Form::submit('Import',['class'=>'btn btn-success']) !!}
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
