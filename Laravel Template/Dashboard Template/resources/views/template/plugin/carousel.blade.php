@extends("template.layout.".env("APP_LAYOUT", "default"))

@section('head')

<link href="/css/component/carousel.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
<link rel="stylesheet" type="text/css" href="/css/plugin/slick.css">
<link rel="stylesheet" type="text/css" href="/css/plugin/slick-theme.css">

<script src="/js/plugin/jquery2.2.min.js" type="text/javascript"></script>
<script src="/js/plugin/slick.min.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript" src="/js/component/carousel.js"></script>

@endsection

@section('content')

<div class="row">

    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <h4 class="mb-3">Slick VS 4 </h4>
                <section class="vertical-center-4 slider">
                    <div>
                        <img src="http://placehold.it/350x100?text=1">
                    </div>
                    <div>
                        <img src="http://placehold.it/350x100?text=2">
                    </div>
                    <div>
                        <img src="http://placehold.it/350x100?text=3">
                    </div>
                    <div>
                        <img src="http://placehold.it/350x100?text=4">
                    </div>
                    <div>
                        <img src="http://placehold.it/350x100?text=5">
                    </div>
                    <div>
                        <img src="http://placehold.it/350x100?text=6">
                    </div>
                    <div>
                        <img src="http://placehold.it/350x100?text=7">
                    </div>
                    <div>
                        <img src="http://placehold.it/350x100?text=8">
                    </div>
                    <div>
                        <img src="http://placehold.it/350x100?text=9">
                    </div>
                    <div>
                        <img src="http://placehold.it/350x100?text=10">
                    </div>
                </section>
            </div>
        </div>
    </div>

</div>

<div class="row">

    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <h4 class="mb-3">Slick VC 3 </h4>
                <section class="vertical-center-3 slider">
                    <div>
                        <img src="http://placehold.it/350x100?text=1">
                    </div>
                    <div>
                        <img src="http://placehold.it/350x100?text=2">
                    </div>
                    <div>
                        <img src="http://placehold.it/350x100?text=3">
                    </div>
                    <div>
                        <img src="http://placehold.it/350x100?text=4">
                    </div>
                    <div>
                        <img src="http://placehold.it/350x100?text=5">
                    </div>
                    <div>
                        <img src="http://placehold.it/350x100?text=6">
                    </div>
                    <div>
                        <img src="http://placehold.it/350x100?text=7">
                    </div>
                    <div>
                        <img src="http://placehold.it/350x100?text=8">
                    </div>
                    <div>
                        <img src="http://placehold.it/350x100?text=9">
                    </div>
                    <div>
                        <img src="http://placehold.it/350x100?text=10">
                    </div>
                </section>
            </div>
        </div>
    </div>

</div>

<div class="row">

    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <h4 class="mb-3">Slick VC 2 </h4>
                <section class="vertical-center-2 slider">
                    <div>
                        <img src="http://placehold.it/350x100?text=1">
                    </div>
                    <div>
                        <img src="http://placehold.it/350x100?text=2">
                    </div>
                    <div>
                        <img src="http://placehold.it/350x100?text=3">
                    </div>
                    <div>
                        <img src="http://placehold.it/350x100?text=4">
                    </div>
                    <div>
                        <img src="http://placehold.it/350x100?text=5">
                    </div>
                    <div>
                        <img src="http://placehold.it/350x100?text=6">
                    </div>
                    <div>
                        <img src="http://placehold.it/350x100?text=7">
                    </div>
                    <div>
                        <img src="http://placehold.it/350x100?text=8">
                    </div>
                    <div>
                        <img src="http://placehold.it/350x100?text=9">
                    </div>
                    <div>
                        <img src="http://placehold.it/350x100?text=10">
                    </div>
                </section>
            </div>
        </div>
    </div>

</div>

<div class="row">

    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <h4 class="mb-3">Slick VC 1 </h4>
                <section class="vertical-center slider">
                    <div>
                        <img src="http://placehold.it/350x100?text=1">
                    </div>
                    <div>
                        <img src="http://placehold.it/350x100?text=2">
                    </div>
                    <div>
                        <img src="http://placehold.it/350x100?text=3">
                    </div>
                    <div>
                        <img src="http://placehold.it/350x100?text=4">
                    </div>
                    <div>
                        <img src="http://placehold.it/350x100?text=5">
                    </div>
                    <div>
                        <img src="http://placehold.it/350x100?text=6">
                    </div>
                    <div>
                        <img src="http://placehold.it/350x100?text=7">
                    </div>
                    <div>
                        <img src="http://placehold.it/350x100?text=8">
                    </div>
                    <div>
                        <img src="http://placehold.it/350x100?text=9">
                    </div>
                    <div>
                        <img src="http://placehold.it/350x100?text=10">
                    </div>
                </section>
            </div>
        </div>
    </div>

</div>

<div class="row">

    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <h4 class="mb-3">Slick Vertical Slider </h4>
                <section class="vertical slider">
                    <div>
                        <img src="http://placehold.it/350x100?text=1">
                    </div>
                    <div>
                        <img src="http://placehold.it/350x100?text=2">
                    </div>
                    <div>
                        <img src="http://placehold.it/350x100?text=3">
                    </div>
                    <div>
                        <img src="http://placehold.it/350x100?text=4">
                    </div>
                    <div>
                        <img src="http://placehold.it/350x100?text=5">
                    </div>
                    <div>
                        <img src="http://placehold.it/350x100?text=6">
                    </div>
                    <div>
                        <img src="http://placehold.it/350x100?text=7">
                    </div>
                    <div>
                        <img src="http://placehold.it/350x100?text=8">
                    </div>
                    <div>
                        <img src="http://placehold.it/350x100?text=9">
                    </div>
                    <div>
                        <img src="http://placehold.it/350x100?text=10">
                    </div>
                </section>
            </div>
        </div>
    </div>

</div>

{{-- <div class="row">

    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <h4 class="mb-3">Regular </h4>
                <section class="regular slider">
                    <div>
                        <img src="http://placehold.it/350x300?text=1">
                    </div>
                    <div>
                        <img src="http://placehold.it/350x300?text=2">
                    </div>
                    <div>
                        <img src="http://placehold.it/350x300?text=3">
                    </div>
                    <div>
                        <img src="http://placehold.it/350x300?text=4">
                    </div>
                    <div>
                        <img src="http://placehold.it/350x300?text=5">
                    </div>
                    <div>
                        <img src="http://placehold.it/350x300?text=6">
                    </div>
                </section>
            </div>
        </div>
    </div>

</div>

<div class="row">

    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <h4 class="mb-3">Glyphicons </h4>
                <section class="center slider">
                    <div>
                        <img src="http://placehold.it/350x300?text=1">
                    </div>
                    <div>
                        <img src="http://placehold.it/350x300?text=2">
                    </div>
                    <div>
                        <img src="http://placehold.it/350x300?text=3">
                    </div>
                    <div>
                        <img src="http://placehold.it/350x300?text=4">
                    </div>
                    <div>
                        <img src="http://placehold.it/350x300?text=5">
                    </div>
                    <div>
                        <img src="http://placehold.it/350x300?text=6">
                    </div>
                    <div>
                        <img src="http://placehold.it/350x300?text=7">
                    </div>
                    <div>
                        <img src="http://placehold.it/350x300?text=8">
                    </div>
                    <div>
                        <img src="http://placehold.it/350x300?text=9">
                    </div>
                </section>
            </div>
        </div>
    </div>

</div>

<div class="row">

    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <h4 class="mb-3">Glyphicons </h4>
                <section class="variable slider">
                    <div>
                        <img src="http://placehold.it/350x300?text=1">
                    </div>
                    <div>
                        <img src="http://placehold.it/200x300?text=2">
                    </div>
                    <div>
                        <img src="http://placehold.it/100x300?text=3">
                    </div>
                    <div>
                        <img src="http://placehold.it/200x300?text=4">
                    </div>
                    <div>
                        <img src="http://placehold.it/350x300?text=5">
                    </div>
                    <div>
                        <img src="http://placehold.it/300x300?text=6">
                    </div>
                </section>
            </div>
        </div>
    </div>

</div>

<div class="row">

    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <h4 class="mb-3">Glyphicons </h4>
                <section class="lazy slider" data-sizes="50vw">
                    <div>
                        <img data-lazy="http://placehold.it/350x300?text=1-350w" data-srcset="http://placehold.it/650x300?text=1-650w 650w, http://placehold.it/960x300?text=1-960w 960w" data-sizes="100vw">
                    </div>
                    <div>
                        <img data-lazy="http://placehold.it/350x300?text=2-350w" data-srcset="http://placehold.it/650x300?text=2-650w 650w, http://placehold.it/960x300?text=2-960w 960w" data-sizes="100vw">
                    </div>
                    <div>
                        <img data-lazy="http://placehold.it/350x300?text=3-350w"  data-srcset="http://placehold.it/650x300?text=3-650w 650w, http://placehold.it/960x300?text=3-960w 960w" data-sizes="100vw">
                    </div>
                    <div>
                        <img data-lazy="http://placehold.it/350x300?text=4-350w"  data-srcset="http://placehold.it/650x300?text=4-650w 650w, http://placehold.it/960x300?text=4-960w 960w" data-sizes="100vw">
                    </div>
                    <div>
                        <img data-lazy="http://placehold.it/350x300?text=5-350w"  data-srcset="http://placehold.it/650x300?text=5-650w 650w, http://placehold.it/960x300?text=5-960w 960w" data-sizes="100vw">
                    </div>
                    <div>
                        <!-- this slide should inherit the sizes attr from the parent slider -->
                        <img data-lazy="http://placehold.it/350x300?text=6-350w"  data-srcset="http://placehold.it/650x300?text=6-650w 650w, http://placehold.it/960x300?text=6-960w 960w">
                    </div>
                </section>
            </div>
        </div>
    </div>

</div> --}}

<div class="row">

    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <h4 class="mb-3">Slick Vertical Slider </h4>
            </div>
        </div>
    </div>

</div>


@stop
