@extends("template.layout.".env("APP_LAYOUT", "default"))

@section('head')
<link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.1/dist/leaflet.css"
integrity="sha512-Rksm5RenBEKSKFjgI3a41vrjkw4EVPlJ3+OiI65vTjIdo9brlAacEuKOiQ5OFh7cOI1bkDwLqdLw3Zg0cRJAAQ=="
crossorigin=""/>
<link href="/css/component/map.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>

<script src="https://unpkg.com/leaflet@1.3.1/dist/leaflet.js"
integrity="sha512-/Nsx9X4HebavoBvEBuyp3I7od5tA0UzAxs+j83KgC8PU0kgB4XiK4Lfe4y4cgBtaRJQEIFCW+oC506aPT2L1zw=="
crossorigin=""></script>
<script type="text/javascript" src="/js/component/map.js{{ config('app.link_version') }}"></script>
@endsection

@section('content')

    <div class='col-lg-12 map-panel'>
        <p> <i class=' ti-map-alt' > </i> Basic Map </p>
        <div id="mapid"></div>
    </div>

@stop
