@extends("template.layout.".env("APP_LAYOUT", "default"))

@section('head')

<link href="/css/component/popup.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
<link rel="stylesheet" type="text/css" href="/css/plugin/sweetalert2.min.css">
<script type="text/javascript" src="/js/plugin/sweetalert2.all.min.js"></script>
<script type="text/javascript" src="/js/component/popup.js"></script>

<script src="/js/plugin/vex.combined.min.js"></script>
<link rel="stylesheet" href="/css/plugin/vex.css" />
<link rel="stylesheet" href="/css/plugin/vex-theme-os.css" />
<link rel="stylesheet" href="/css/plugin/vex-theme-default.css" />
<link rel="stylesheet" href="/css/plugin/vex-theme-plain.css" />
<link rel="stylesheet" href="/css/plugin/vex-theme-wireframe.css" />
<link rel="stylesheet" href="/css/plugin/vex-theme-flat-attack.css" />
<link rel="stylesheet" href="/css/plugin/vex-theme-top.css" />
<link rel="stylesheet" href="/css/plugin/vex-theme-bottom-right-corner.css" />

@endsection

@section('content')
<div class="row">

    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <h4 class="mb-3">Sweet Alert 2 </h4>
                <button id="normal" class="btn btn-primary">Normal</button>
                <button id="success" class="btn btn-success">Success</button>
                <button id="error" class="btn btn-danger">Error</button>
                <button id="warning" class="btn btn-warning">Warning</button>
                <button id="info" class="btn btn-info">Info</button>
                <button id="question" class="btn btn-default">Question</button>
            </div>
        </div>
    </div>

</div>

<div class="row">

    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <h4 class="mb-3">Bootstrap Modal</h4>
                <!-- Button trigger modal -->
                <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">
                    Launch demo modal
                </button>

                <!-- Modal -->
                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="myModalLabel">Dota 2 Update - October 31st, 2018</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            </div>
                            <div class="modal-body">
                                <p>- Fixed a longstanding bug that caused units to attack more slowly than expected at certain attack speeds.</p>
                                <p>- Fixing a bug that caused Morphling to not gain stats when leveling up while morphed.</p>
                                <p>- Fixed a bug that allowed the Silence from Riki's Smoke Screen to be applied to units with Spell Immunity.</p>
                                <p>- Fixed a bug that caused Monkey King to become disabled when Feared during Primal Spring channeling.</p>
                                <p>- Fixed post-game scoreboard text not clipping correctly with long strings.</p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-success">Update</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

</div>

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <h4 class="mb-3">Vex Popup</h4>
                <button id="default" class="btn btn-primary">Default</button>
                <button id="os" class="btn btn-success">OS</button>
                <button id="plain" class="btn btn-danger">Plain</button>
                <button id="wireframe" class="btn btn-warning">Wireframe</button>
                <button id="flat" class="btn btn-info">Flat</button>
                <button id="top" class="btn btn-disabled">Top</button>
                <button id="corner" class="btn btn-default">Corner</button>
            </div>
        </div>
    </div>
</div>

@stop
