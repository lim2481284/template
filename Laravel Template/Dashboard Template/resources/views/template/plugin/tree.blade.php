@extends("template.layout.".env("APP_LAYOUT", "default"))

@section('head')
<link href="/css/component/tree-list.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
<link href="/css/component/tree-hierarchy.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>


<script type="text/javascript" src="/js/plugin/easyTree.min.js{{ config('app.link_version') }}"></script>
<script type="text/javascript" src="/js/component/tree-list.js{{ config('app.link_version') }}"></script>
<script type="text/javascript" src="/js/plugin/tree-hierarchy.js{{ config('app.link_version') }}"></script>
<script type="text/javascript" src="/js/component/tree-hierarchy.js{{ config('app.link_version') }}"></script>
@endsection

@section('content')
<ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
    <li class="nav-item">
        <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#tree-list" >Tree List </a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#tree-hierarchy" >Tree Hierarchy </a>
    </li>
</ul>
<div class="tab-content" id="pills-tabContent">
    <div class="tab-pane fade show active" id="tree-list">
        <div class='tree-list-box'>
            <h3 class='title'>Tree title</h3>
            <div class="easy-tree">
                <ul>
                    <li>Username 1 </li>
                    <li>Username 1  2</li>
                    <li>Example 3
                        <ul>
                            <li>Example 1</li>
                            <li>Username 1 Username 1  2
                                <ul>
                                    <li>Username 1 Username 1  1</li>
                                    <li>Example 2</li>
                                    <li>Example 3</li>
                                    <li>Example 4</li>
                                </ul>
                            </li>
                            <li>Example 3
                                <ul>
                                    <li>Example 1</li>
                                    <li>Example 2</li>
                                    <li>Example 3</li>
                                    <li>Example 4
                                        <ul>
                                            <li>Example 1</li>
                                            <li>Example 2</li>
                                            <li>Example 3</li>
                                            <li>Example 4</li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>

                            <li>Example 4</li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="tab-pane fade" id="tree-hierarchy">
        <div class='tree-hierarchy-box'>
        </div>
    </div>
</div>
@stop
