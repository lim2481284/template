<style>
    :root {

        /* APP SETTING */
        --app-logo-width: {{ env('APP_LOGO_WIDTH') }}px;
        --app-logo-small-width: {{ env('APP_LOGO_SMALL_WIDTH') }}px;


        /* FONT SETTING */
        --font-primary-family: url(/fonts/{{ env('FONT_PRIMARY_FAMILY') }});
        --font-secondary-family:  url(/fonts/{{ env('FONT_SECONDAY_FAMILY') }});
        --font-h1-size: {{ env('FONT_H1_SIZE') }}px;
        --font-h2-size: {{ env('FONT_H2_SIZE') }}px;
        --font-h3-size: {{ env('FONT_H3_SIZE') }}px;
        --font-h4-size: {{ env('FONT_H4_SIZE') }}px;
        --font-h5-size: {{ env('FONT_H5_SIZE') }}px;
        --font-p-size: {{ env('FONT_P_SIZE') }}px;
        --font-small-size: {{ env('FONT_SMALL_SIZE') }}px;


        /* COLOR SETTING */
        --primary-theme: #{{ env('PRIMARY_COLOR') }};
        --secondary-theme: #{{ env('SECONDARY_COLOR') }};
        --third-theme: #{{ env('THIRD_COLOR') }};
        --warning-color: #{{ env('WARNING_COLOR') }};
        --info-color: #{{ env('INFO_COLOR') }};
        --font-primary-color: #{{ env('FONT_PRIMARY_COLOR') }};
        --font-secondary-color: #{{ env('FONT_SECONDARY_COLOR') }};
        --font-inactive-color: #{{ env('FONT_INACTIVE_COLOR') }};
        --font-active-color: #{{ env('FONT_ACTIVE_COLOR') }};

    }
</style>
