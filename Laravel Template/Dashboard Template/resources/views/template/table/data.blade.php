@extends("template.layout.".env("APP_LAYOUT", "default"))

@section('head')
<link href="/css/plugin/uploadBox.min.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
<link href="/css/component/modal.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
<link href="/css/component/table.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
<link href="/css/plugin/flatpickr.min.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
<script type="text/javascript" src="/js/plugin/uploadBox.min.js{{ config('app.link_version') }}"></script>
<script type="text/javascript" src="/js/plugin/flatpickr.min.js{{ config('app.link_version') }}"></script>
<script type="text/javascript" src="/js/component/table.js{{ config('app.link_version') }}"></script>
@endsection

@section('content')
<div class='table-action-section'>
    {!! Form::open(['route' => 'template.datatable.search']) !!}
    <div class="input-group col-lg-4 no-padding-left">
        <input type="text" class="form-control search-input" name='searchQuery' placeholder="{{($query)?$query:'Search ...'}}." >
        <div class="input-group-append">
            <button class="btn btn-default search-btn" type="submit" >
                <i class='ti-search'> </i>
            </button>
        </div>
    </div>
    {!! Form::close() !!}
    <button class='btn btn-default'> Advanced Search </button>
    <div class="dropdown">
        <button class=" btn btn-primary">Action</button>
        <div class="dropdown-content">
            <a href="#"  data-toggle="modal" data-target="#exportCSVModal">Export to CSV</a>
            <a href="#" data-toggle="modal" data-target="#exportPDFModal">Export to PDF</a>
            <a href="#" class='division' data-toggle="modal" data-target="#importModal">Import CSV</a>
        </div>
    </div>
</div>
<div class='table-section'>
    <h3 class='title'> Table title </h3>
    <p class='subtitle'> Hold "Shift" and scroll for horizontal scroll </p>
    <div class="table-responsive">
        <table class="table">
            <thead class='thead-green'>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Data</th>
                    <th scope="col">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach($results as $result)
                    <tr>
                        <th scope="row">1</th>
                        <td>{{$result->name}}</td>
                        <td>{{$result->data}}</td>
                        <td>@mdo</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        {{ $results->appends(['searchQuery' => $query])->links() }}
        <p class='search-total'> {{$total}} Records</p>
    </div>
</div>

@include('template.modal.export')
@include('template.modal.import')
@stop
