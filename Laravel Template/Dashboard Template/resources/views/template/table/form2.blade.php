@extends("template.layout.".env("APP_LAYOUT", "default"))

@section('head')
<link href="/css/component/modal.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
<link href="/css/component/table.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
<script type="text/javascript" src="/js/component/table.js{{ config('app.link_version') }}"></script>
@endsection

@section('content')

<div class='table-section'>
    <div class='inline-table-form-section'>
        <i class='ti-book icon-red'> </i>
        <h3 class='title'>Form Title</h3>

        <div class="form-group row">
            <label for="example-week-input" class="col-12 col-sm-2 col-form-label">Week</label>
            <div class="col-12 col-sm-10">
                <input class="form-control" type="week" value="2011-W33" id="example-week-input">
            </div>
        </div>
        <div class="form-group row">
            <label for="example-time-input" class="col-12 col-sm-2 col-form-label">Time</label>
            <div class="col-12 col-sm-10">
                <input class="form-control" type="time" value="13:45:00" id="example-time-input">
            </div>
        </div>
        <button class='btn btn-primary'> Search </button>
        <button class='btn btn-default'> Cancel </button>
    </div>
    <h3 class='title'> Table title </h3>
    <p class='subtitle'> Hold "Shift" and scroll for horizontal scroll </p>
    <div class="table-responsive">
        <table class="table">
            <thead class='thead-green'>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Data</th>
                    <th scope="col">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach($results as $result)
                    <tr>
                        <th scope="row">1</th>
                        <td>{{$result->name}}</td>
                        <td>{{$result->data}}</td>
                        <td>@mdo</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        {{ $results->appends(['searchQuery' => $query])->links() }}
        <p class='search-total'> {{$total}} Records</p>
    </div>
</div>

@stop
