@extends("template.layout.".env("APP_LAYOUT", "default"))

@section('head')
<link href="/css/dashboard/component/table.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
<script type="text/javascript" src="/js/dashboard/component/table.js{{ config('app.link_version') }}"></script>
@endsection

@section('content')
<div class='table-section'>
    <h3 class='title'> CSV data record </h3>
    <p class='subtitle'> Hold "Shift" and scroll for horizontal scroll </p>
    <div class="table-responsive">
        <table class="table">
            <thead class='thead-none'>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Data</th>
                </tr>
            </thead>
            <tbody>
                @foreach($row as $key=>$record)
                    <tr>
                        <td>{{ ++$key }}</td>
                        <td>{{$record[0]}}</td>
                        <td>{{$record[1]}}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        <div class='col-sm-12 confirm-section'>


        </div>
    </div>
</div>
<div class='row'>
    <div class="col-sm-6 col-lg-6 col-xl-12">
        <div class='table-form-section'>
            <i class='ti-info-alt icon-red'> </i>
            <h4 class='small-title'>Are you sure you want to import this csv into database ? </h4>
            <br>
            {!! Form::open(['route' => 'dashboard.datatable.import', 'class'=>'inline-block']) !!}
                <input type='hidden' name='path' value="{{$path}}"/>
                {!! Form::submit('Confirm',['class'=>'btn btn-success']) !!}
            {!! Form::close() !!}
            {!! Form::open(['route' => 'dashboard.datatable.import.cancel', 'class'=>'inline-block']) !!}
                <input type='hidden' name='path' value="{{$path}}"/>
                {!! Form::submit('Cancel',['class'=>'btn btn-danger']) !!}
            {!! Form::close() !!}
        </div>
    </div>
</div>
@stop
