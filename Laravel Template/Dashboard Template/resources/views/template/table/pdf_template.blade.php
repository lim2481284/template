<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<style>
h1,p{
    font-family: Arial, Helvetica, sans-serif;
    font-size: 24px;
    font-weight: 600;
}
p{
    font-size: 13px;
    font-weight: 300;
    color:#6F6F6F;
}
h1{
    color:#3B3B3B;
}
hr{
    border:none;
    border-top: 1px solid #8c8b8b;
}
.page-break {
    page-break-after: always;
}
</style>

@foreach($records as $record)

    <h1>        
        ID : {{$record->id}}
    </h1>
    <hr>
    <p> Name : {{$record->name}}</p>
    <p> Data : {{$record->data}}</p>
    <div class="page-break"></div>

@endforeach
