<?php

//Localization
Route::group(['prefix' => LaravelLocalization::setLocale(),'middleware' => [ 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath' ]],function()
{

    //Redirect to template
    Route::get('/', ['as' => 'template.index', 'uses' => 'Template\IndexController@index']);

    //Dashboard route
    Route::group(['prefix'=>'template','as'=>'template.', 'namespace'=>'Template'],function () {

        //Index page
        Route::get('/', ['as' => 'index', 'uses' => 'IndexController@index']);

        //Login page
        Route::get('/login', ['as' => 'index', 'uses' => 'AuthController@loginPage']);

        //UI component
        Route::group(['prefix'=>'/component','as'=>'component.'],function () {
            Route::get('/widget', ['as' => 'widget', 'uses' => 'ComponentController@componentWidget']);
            Route::get('/button', ['as' => 'button', 'uses' => 'ComponentController@componentButton']);
            Route::get('/tab', ['as' => 'tab', 'uses' => 'ComponentController@componentTab']);
            Route::get('/list', ['as' => 'list', 'uses' => 'ComponentController@componentList']);
            Route::get('/card', ['as' => 'card', 'uses' => 'ComponentController@componentCard']);
            Route::get('/typo', ['as' => 'typo', 'uses' => 'ComponentController@componentTypo']);
            Route::get('/modal', ['as' => 'modal', 'uses' => 'ComponentController@componentModal']);
            Route::get('/input', ['as' => 'input', 'uses' => 'ComponentController@componentInput']);
            Route::get('/scroll', ['as' => 'scroll', 'uses' => 'ComponentController@componentScroll']);
        });


        //Table
        Route::group(['prefix'=>'/table','as'=>'table.'],function () {
            Route::get('/basic', ['as' => 'basic', 'uses' => 'TableController@basicTable']);
            Route::get('/form', ['as' => 'form', 'uses' => 'TableController@formTable']);
            Route::get('/form/2', ['as' => 'form2', 'uses' => 'TableController@formTable2']);
            Route::post('/data', ['as' => 'datatable.search', 'uses' => 'TableController@dataTable']);
            Route::post('/data/exportPDF', ['as' => 'datatable.exportpdf', 'uses' => 'TableController@exportPDF']);
            Route::post('/data/exportCSV', ['as' => 'datatable.exportcsv', 'uses' => 'TableController@exportCSV']);
            Route::post('/data/import', ['as' => 'datatable.import', 'uses' => 'TableController@importCSV']);
            Route::post('/data/import/cancel', ['as' => 'datatable.import.cancel', 'uses' => 'TableController@cancelImport']);
            Route::post('/data/import/view', ['as' => 'datatable.import.view', 'uses' => 'TableController@importCSVView']);
        });


        //Form
        Route::group(['prefix'=>'/form','as'=>'form.'],function () {
            Route::get('/basic', ['as' => 'basic', 'uses' => 'FormController@basicForm']);
            Route::get('/advanced', ['as' => 'advanced', 'uses' => 'FormController@advancedForm']);
            Route::post('/advanced/fileupload', ['as' => 'fileupload', 'uses' => 'FormController@fileUpload']);
        });


        //Plugin
        Route::group(['prefix'=>'/plugin','as'=>'plugin.'],function () {
            Route::get('/chart', ['as' => 'chart', 'uses' => 'PluginController@chart']);
            Route::get('/map', ['as' => 'map', 'uses' => 'PluginController@map']);
            Route::get('/tree', ['as' => 'tree', 'uses' => 'PluginController@tree']);
            Route::get('/icons', ['as' => 'icons', 'uses' => 'PluginController@icons']);
            Route::get('/popup', ['as' => 'popup', 'uses' => 'PluginController@popup']);
            Route::get('/carousel', ['as' => 'carousel', 'uses' => 'PluginController@carousel']);

        });

    });

});
