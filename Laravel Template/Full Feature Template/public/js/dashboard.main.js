
$(document).ready(function(){

    $('.profile-tooltip').tooltipster({
       animation: 'fade',
       delay: 0,
       trigger: 'click',
       contentCloning : true,
       interactive : true
   });

    $('.language-tooltip').tooltipster({
       animation: 'fade',
       delay: 0
    });

    //Menu toggle
    $('#menuToggle').on('click', function(event) {
        $('body').toggleClass('open');
    });

    //Search trigger
    $('.search-trigger').on('click', function(event) {
        event.preventDefault();
        event.stopPropagation();
        $('.search-trigger').parent('.header-left').addClass('open');
    });
    $('.search-close').on('click', function(event) {
        event.preventDefault();
        event.stopPropagation();
        $('.search-trigger').parent('.header-left').removeClass('open');
    });

    //Page loader
    $('.page-loader').fadeOut('slow');
})
