<?php
   return [
      'web_title'           => 'iChainPay',
      'choose_payment'      => 'Choose a payment method to load your card',
      'first_name'          => 'First Name',
      'last_name'           => 'Last Name',
      'acc_num'             => 'Card Number',
      'amount_in_bitcoin'    => 'Amount in Bitcoin',
      'email'               => 'Email Address',
      'phone'               => 'Phone Number',
      'bitcoin'             => 'Bitcoin',
      'payment'             => 'payment',
      'complete_payment'    => 'Complete Payment',
      'back'                => 'Back',
      'amount_in_usd'       => 'Amount in USD'
   ];
?>
