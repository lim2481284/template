<?php
   return [
       'web_title'           => 'iChainPay',
       'choose_payment'      => '请选择付款方式',
       'first_name'          => '姓',
       'last_name'           => '名字',
       'acc_num'             => '卡片号码',
       'amount_in_bitcoin'    => '美元对换比特币',
       'email'               => '电子邮件',
       'phone'               => '电话号码',
       'bitcoin'             => '比特币',
       'payment'             => '付款',
       'complete_payment'    => '完成付款',
       'back'                => '返回',
       'amount_in_usd'       => '美元金额'
   ];
?>
