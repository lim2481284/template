<!doctype html>
<html lang="{{ app()->getLocale() }}">

<head>
    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>Dashboard</title>
    <!-- Meta End-->

    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Raleway" />
    <link rel="shortcut icon" href="/img/icon/pfe.ico"/>
    <link href="/css/plugin.bootstrap.min.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
    <link href="/css/plugin.normalize.min.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
    <link href="/css/plugin.fontawesome.min.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
    <link href="/css/plugin.themify-icons.min.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="/css/plugin.tooltipster.bundle.min.css" />
    <link href="/css/dashboard.main.min.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
    <link href="/css/dashboard.responsive.min.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
    <!-- Styles end -->

    <!-- Script -->
    <script type="text/javascript" src="/js/plugin.jquery.min.js{{ config('app.link_version') }}"></script>
    <script type="text/javascript" src="/js/plugin.tooltipster.bundle.min.js{{ config('app.link_version') }}"></script>
    <script type="text/javascript" src="/js/plugin.bootstrap.min.js{{ config('app.link_version') }}"></script>
    <script type="text/javascript" src="/js/plugin.plugins.min.js{{ config('app.link_version') }}"></script>
    <script type="text/javascript" src="/js/dashboard.main.min.js{{ config('app.link_version') }}"></script>
    <!-- Script End -->

    @yield('head')
</head>

<body>
    <!--Loader section -->
    <div class='page-loader'>
        <div class='loader'>
            <img src='/img/icon/loader.gif'/>
            <label class='small'> Loading </label>
        </div>
    </div>
    <!--Loader section end -->

    <!-- Menu Panel -->
    <aside id="left-panel" class="left-panel">
        <nav class="navbar navbar-expand-sm navbar-default">

            <div class="navbar-header">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand" href="./"><img src="/img/logo/logo.png" alt="Logo"></a>
                <a class="navbar-brand hidden" href="./"><img src="/img/logo/logo_small.png" alt="Logo"></a>
            </div>

            <div id="main-menu" class="main-menu collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li class="active">
                        <a href='/dashboard'>
                            <i class="menu-icon icon-red ti-home"></i>
                            Home
                        </a>
                    </li>
                    <li>
                        <a href='/dashboard'>
                            <i class="menu-icon icon-green ti-user"></i>
                            Member
                        </a>
                    </li>
                    <li>
                        <a href='/dashboard'>
                            <i class="menu-icon icon-purple ti-star"></i>
                            Activation
                        </a>
                    </li>
                    <li>
                        <a href='/dashboard'>
                            <i class="menu-icon icon-blue ti-stats-up"></i>
                            Transaction
                        </a>
                    </li>

                    <!-- Childeren menu
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-table"></i>Tables</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-table"></i><a href="tables-basic.html">Basic Table</a></li>
                            <li><i class="fa fa-table"></i><a href="tables-data.html">Data Table</a></li>
                        </ul>
                    </li>
                    -->
                    <li>
                        <a href="index.html"> <i class="menu-icon icon-orange ti-clipboard"></i>Report  </a>
                    </li>
                    <li>
                        <a href="index.html"> <i class="menu-icon icon-lightblue ti-reload"></i>History  </a>
                    </li>
                </ul>
            </div>
        </nav>
    </aside>
    <!-- Menu Panel End-->

    <!-- Content Panel -->
    <div id="right-panel" class="right-panel">

        <!-- Header -->
        <header id="header" class="header">
            <div class="header-menu">

                <!-- Searchbar -->
                <div class="col-sm-7">
                    <a id="menuToggle" class="menutoggle pull-left"><i class="fa fa fa-tasks"></i></a>
                </div>
                <!-- Searchbar End -->

                <div class="col-sm-5">
                    <!-- Profile -->
                    <div class="user-area dropdown float-right">
                        {{ Form::select('localization_language', getSupportedLocalesNative(), LaravelLocalization::getLocalizedURL(LaravelLocalization::getCurrentLocale() , null, [], true)  , ['class' => 'form-control localization_language col-centered']) }}
                        <label class='username'> Username </label>
                        <a href="#" class="" aria-haspopup="true" aria-expanded="false" >
                            <img class="user-avatar rounded-circle profile-tooltip" src="/img/icon/profile.png" alt="User Avatar"  data-tooltip-content="#tooltip_content">
                        </a>
                        <div class="tooltip_templates">
                            <span id="tooltip_content">
                                <a href="/dashboard/profile"> <i class="menu-icon icon-lightpurple ti-user"></i>Profile  </a>
                                <a href="/logout"> <i class="menu-icon icon-lightred ti-share"></i>Logout  </a>
                            </span>
                        </div>
                    </div>
                    <!-- Profile End -->

                    <!-- Language  -->
                    <div class="language-select dropdown" id="language-select">
                        <a class="dropdown-toggle" href="#" data-toggle="dropdown"  id="language" aria-haspopup="true" aria-expanded="true">
                            <i class="flag-icon flag-icon-us"></i>
                        </a>
                        <div class="dropdown-menu" aria-labelledby="language" >
                            <div class="dropdown-item">
                                <span class="flag-icon flag-icon-fr"></span>
                            </div>
                            <div class="dropdown-item">
                                <i class="flag-icon flag-icon-es"></i>
                            </div>
                            <div class="dropdown-item">
                                <i class="flag-icon flag-icon-us"></i>
                            </div>
                            <div class="dropdown-item">
                                <i class="flag-icon flag-icon-it"></i>
                            </div>
                        </div>
                    </div>
                    <!-- Language End -->

                </div>
            </div>
        </header>
        <!-- Header End-->

        <!-- Body Content -->
        <div class="content mt-3">
            @yield('content')
        </div>
        <!-- Body Content End-->

    </div>
    <!-- Content Panel End-->

</body>
</html>
