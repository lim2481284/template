<?php

//Localization
Route::group(['prefix' => LaravelLocalization::setLocale(),'middleware' => [ 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath' ]],function()
{

    //Dashboard route
    Route::prefix('dashboard')->group(function () {

        Route::get('/', function () {
            return view('dashboard.index');
        });

    });
    
});
