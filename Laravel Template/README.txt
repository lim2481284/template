=========================================================================
Blank Template
=========================================================================
- Custom helper
- Script and style custom config version
- Laravel form
- Template command console 


=========================================================================
Standard Info Website Template
=========================================================================
- Custom helper
- Script and style custom config version
- Localization + Automated localization
- Website standard code


=========================================================================
Standard Website & Dashboard Template
=========================================================================
- Custom helper
- Script and style custom config version
- Localization + Automated localization
- Website standard code


=========================================================================
Membership Website & Dashboard Template
=========================================================================
- Custom helper
- Script and style custom config version
- Localization + Automated localization
- Website standard code


=========================================================================
Booking Website & Dashboard Template
=========================================================================
- Custom helper
- Script and style custom config version
- Localization + Automated localization
- Authentication
  - Email validation
  - phone pattern validation
- Website standard code
- Autocomplete searching
- Pagination


=========================================================================
Standard Dashboard Template
=========================================================================
- Custom helper
- Script and style custom config version
- Localization + Automated localization
- Authentication
  - Email validation
  - phone pattern validation
- Dashboard standard code
- Storage Link
- Mail service


=========================================================================
Full Feature Template
=========================================================================
- Custom helper
- Script and style custom config version
- Localization + Automated localization
- Authentication
  - Email validation
  - phone pattern validation
- Routing document : group , auth, prefix ...
- Website standard code
- Dashboard standard code
- Autocomplete Search Code
- API Document
- Policy and Gateway
- Testing Framework
- Storage Link
- Mail service
- Scheduler & Console command
- Payment gateway
- Subscription
- Laravel form
- Code sample
    - Eloquent relationship code
    - Factory code
    - Seeds code
    - Laravel form code
    - Migrate code with relationship
