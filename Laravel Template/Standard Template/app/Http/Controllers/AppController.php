<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class AppController extends Controller
{
    //Homepage
    public function index(Request $request){
        $language = LaravelLocalization::getCurrentLocale();
        return view('app.index');
    }

    //Profile
    public function profile(Request $request){
        $language = LaravelLocalization::getCurrentLocale();
        return view('app.profile');
    }
}
