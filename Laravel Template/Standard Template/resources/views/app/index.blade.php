@extends('app.layout')

@section('head')
    <link href="/css/app.homepage.min.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
    <script type="text/javascript" src="/js/app.homepage.min.js{{ config('app.link_version') }}"></script>
@endsection

@section('content')

@stop
