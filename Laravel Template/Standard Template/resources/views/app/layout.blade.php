<!doctype html>
<html lang="{{ app()->getLocale() }}">

<head>

    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>Title</title>
    <!-- Meta End-->

    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Raleway" />
    <link rel="shortcut icon" href="/img/icon/pfe.ico"/>
    <link href="/css/plugin.bootstrap.min.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
    <link href="/css/plugin.fontawesome.min.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
    <link href="/css/plugin.animate.min.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
    <link href="/css/app.main.min.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
    <link href="/css/app.responsive.min.css{{ config('app.link_version') }}" type="text/css" rel="stylesheet"/>
    <!-- Styles end -->

    <!-- Script -->
    <script type="text/javascript" src="/js/plugin.jquery.min.js{{ config('app.link_version') }}"></script>
    <script type="text/javascript" src="/js/plugin.bootstrap.min.js{{ config('app.link_version') }}"></script>
    <script type="text/javascript" src="/js/plugin.wow.min.js{{ config('app.link_version') }}"></script>
    <script type="text/javascript" src="/js/plugin.sweetalert.min.js{{ config('app.link_version') }}"></script>
    <script type="text/javascript" src="/js/app.main.min.js{{ config('app.link_version') }}"></script>
    <!-- Script End -->

    @yield('head')
</head>

<body>

    <!--Loader section -->
    <div class='page-loader'>
        <div class='loader'>
            <img src='/img/icon/loader.gif'/>
            <label class='small'> Loading </label>
        </div>
    </div>
    <!--Loader section end -->

    <!-- Menu Section -->
    <!-- Menu Section End-->

    <!-- Content Panel -->
        @yield('content')
    <!-- Content Panel End-->

    <!-- Footer Section -->
    <!-- Footer Section End-->

</body>
</html>
