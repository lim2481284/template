<?php

//Logout Route
Route::get('logout', ['as' => 'logout', 'uses' => 'Auth\LoginController@logout']);

//Localization
Route::group(['prefix' => LaravelLocalization::setLocale(),'middleware' => [ 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath' ]],function()
{
    //Authentication Route
    Auth::routes();

    //Homepage Route
    Route::get('/', ['as' => 'index', 'uses' => 'AppController@index']);

    //Authenticated route
    Route::group(['prefix' => 'dashboard',  'middleware' => 'auth'], function()
    {

        //Profile Route
        Route::get('/', ['as' => 'index', 'uses' => 'AppController@profile']);

    });

});
