<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class template extends Command
{
    /**
     * The name and signature of the console command.
     */
    protected $signature = 'template {input?} {option?}';


    /**
     * The console command description.
     */
    protected $description = 'Please run \'php artisan template:help\' command';


    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
        $this->packageArr =  [
            'localization' => "Multiple language localization",
            'currency_localization' => "Multiple currency localization",
            'custom_helper' => "Custom global helper function ",
            'info_web' => "Standard info website",
            'dashboard' => "Standard dashboard",
            'booking_web' => "Standard booking website",
            'authentication' => "Authenticaiton with email verification",
            'mail_service' => "Basic mail service setup"
        ];
    }


    /**
     * Execute the console command.
     */
    public function handle()
    {
        //Handle the argument
        $command = $this->argument('input');
        if(!$command) $command = "help";
        if ($command == "help")
            $this->help();
        else if ($command == "package")
            $this->package();
        else if ($command == "install")
        {
            $this->install();
        }
    }


    /**
     * Run step by step installation guideline
     */
    public function install()
    {
        if($package = $this->argument("option"))
        {
            if(array_key_exists($package,$this->packageArr))
            {
                shell_exec('composer require mcamara/laravel-localization');
                shell_exec('composer require stichoza/google-translate-php');
                shell_exec('git ');
            }
            else
                $this->line("Package Not Found");
        }
        else
        {
            $this->line("Please insert package name");
        }
    }


    /**
     * View the list of package
     */
    public function package()
    {
        $this->line("
                        |     Template package list      |
        localization                Multiple language converter (Auto & Manually convert)
        ");
    }


    /**
     * Display list of package and instruction
     */
    public function help()
    {
        if($package = $this->argument('option'))
        {
            if(array_key_exists($package,$this->packageArr))
            {
                if($package=='localization')
                {
                    $this->line("");
                    $this->line("Localiztion package is the multiple language converter either manually or auto ( default ) convert the word.");
                }
            }
            else
            {
                $this->line("Package not found. Please run 'php artisan template package' command to get the list of package name");
            }

        }
        else
        {
        $this->line("
                    |     Template Command Guideline         |

    php artisan template help               To get the list of command and instruction
    php artisan template help [package]     Find out more info about the package
    php artisan template package            To get the list of package name
    php artisan template install [package]  To install specific template package
        ");
        }
    }
}
